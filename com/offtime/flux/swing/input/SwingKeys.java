package com.offtime.flux.swing.input;

import com.offtime.flux.core.input.IKeyMapper;
import com.offtime.flux.core.input.Keys;

import java.awt.event.KeyEvent;

import static com.offtime.flux.core.input.Keys.*;

/**
 * Created by GeoDoX on 2016-09-20.
 */
public class SwingKeys implements IKeyMapper
{
    @Override
    public void map()
    {
        Keys.map(ENTER,             KeyEvent.VK_ENTER);
        Keys.map(BACKSPACE,         KeyEvent.VK_BACK_SPACE);
        Keys.map(TAB,               KeyEvent.VK_TAB);
        Keys.map(PAUSE,             KeyEvent.VK_PAUSE);
        Keys.map(CAPS_LOCK,         KeyEvent.VK_CAPS_LOCK);
        Keys.map(NUM_LOCK,          KeyEvent.VK_NUM_LOCK);
        Keys.map(SCROLL_LOCK,       KeyEvent.VK_SCROLL_LOCK);
        Keys.map(ESCAPE,            KeyEvent.VK_ESCAPE);
        Keys.map(SPACE,             KeyEvent.VK_SPACE);
        Keys.map(PAGE_UP,           KeyEvent.VK_PAGE_UP);
        Keys.map(PAGE_DOWN,         KeyEvent.VK_PAGE_DOWN);
        Keys.map(END,               KeyEvent.VK_END);
        Keys.map(HOME,              KeyEvent.VK_HOME);
        Keys.map(COMMA,             KeyEvent.VK_COMMA);
        Keys.map(PERIOD,            KeyEvent.VK_PERIOD);
        Keys.map(MINUS,             KeyEvent.VK_MINUS);
        Keys.map(EQUALS,            KeyEvent.VK_EQUALS);
        Keys.map(SLASH,             KeyEvent.VK_SLASH);
        Keys.map(BACKSLASH,         KeyEvent.VK_BACK_SLASH);
        Keys.map(SEMICOLON,         KeyEvent.VK_SEMICOLON);
        Keys.map(OPEN_BRACKET,      KeyEvent.VK_OPEN_BRACKET);
        Keys.map(CLOSE_BRACKET,     KeyEvent.VK_CLOSE_BRACKET);
        Keys.map(DELETE,            KeyEvent.VK_DELETE);
        Keys.map(QUOTE,             KeyEvent.VK_QUOTE);
        Keys.map(BACK_QUOTE,        KeyEvent.VK_BACK_QUOTE);
        Keys.map(PRINT_SCREEN,      KeyEvent.VK_PRINTSCREEN);
        Keys.map(INSERT,            KeyEvent.VK_INSERT);
        Keys.map(OS,                KeyEvent.VK_WINDOWS);

        Keys.map(LEFT,              KeyEvent.VK_LEFT);
        Keys.map(RIGHT,             KeyEvent.VK_RIGHT);
        Keys.map(UP,                KeyEvent.VK_UP);
        Keys.map(DOWN,              KeyEvent.VK_DOWN);
        Keys.map(NUM_PAD_LEFT,      KeyEvent.VK_KP_LEFT);
        Keys.map(NUM_PAD_RIGHT,     KeyEvent.VK_KP_RIGHT);
        Keys.map(NUM_PAD_UP,        KeyEvent.VK_KP_UP);
        Keys.map(NUM_PAD_DOWN,      KeyEvent.VK_KP_DOWN);

        Keys.map(ZERO,              KeyEvent.VK_0);
        Keys.map(ONE,               KeyEvent.VK_1);
        Keys.map(TWO,               KeyEvent.VK_2);
        Keys.map(THREE,             KeyEvent.VK_3);
        Keys.map(FOUR,              KeyEvent.VK_4);
        Keys.map(FIVE,              KeyEvent.VK_5);
        Keys.map(SIX,               KeyEvent.VK_6);
        Keys.map(SEVEN,             KeyEvent.VK_7);
        Keys.map(EIGHT,             KeyEvent.VK_8);
        Keys.map(NINE,              KeyEvent.VK_9);

        Keys.map(NUM_PAD_ZERO,      KeyEvent.VK_NUMPAD0);
        Keys.map(NUM_PAD_ONE,       KeyEvent.VK_NUMPAD1);
        Keys.map(NUM_PAD_TWO,       KeyEvent.VK_NUMPAD2);
        Keys.map(NUM_PAD_THREE,     KeyEvent.VK_NUMPAD3);
        Keys.map(NUM_PAD_FOUR,      KeyEvent.VK_NUMPAD4);
        Keys.map(NUM_PAD_FIVE,      KeyEvent.VK_NUMPAD5);
        Keys.map(NUM_PAD_SIX,       KeyEvent.VK_NUMPAD6);
        Keys.map(NUM_PAD_SEVEN,     KeyEvent.VK_NUMPAD7);
        Keys.map(NUM_PAD_EIGHT,     KeyEvent.VK_NUMPAD8);
        Keys.map(NUM_PAD_NINE,      KeyEvent.VK_NUMPAD9);

        Keys.map(NUM_PAD_MULTIPLY,  KeyEvent.VK_MULTIPLY);
        Keys.map(NUM_PAD_DIVIDE,    KeyEvent.VK_DIVIDE);
        Keys.map(NUM_PAD_ADD,       KeyEvent.VK_ADD);
        Keys.map(NUM_PAD_SUBTRACT,  KeyEvent.VK_SUBTRACT);

        Keys.map(A,                 KeyEvent.VK_A);
        Keys.map(B,                 KeyEvent.VK_B);
        Keys.map(C,                 KeyEvent.VK_C);
        Keys.map(D,                 KeyEvent.VK_D);
        Keys.map(E,                 KeyEvent.VK_E);
        Keys.map(F,                 KeyEvent.VK_F);
        Keys.map(G,                 KeyEvent.VK_G);
        Keys.map(H,                 KeyEvent.VK_H);
        Keys.map(I,                 KeyEvent.VK_I);
        Keys.map(J,                 KeyEvent.VK_J);
        Keys.map(K,                 KeyEvent.VK_K);
        Keys.map(L,                 KeyEvent.VK_L);
        Keys.map(M,                 KeyEvent.VK_M);
        Keys.map(N,                 KeyEvent.VK_N);
        Keys.map(O,                 KeyEvent.VK_O);
        Keys.map(P,                 KeyEvent.VK_P);
        Keys.map(Q,                 KeyEvent.VK_Q);
        Keys.map(R,                 KeyEvent.VK_R);
        Keys.map(S,                 KeyEvent.VK_S);
        Keys.map(T,                 KeyEvent.VK_T);
        Keys.map(U,                 KeyEvent.VK_U);
        Keys.map(V,                 KeyEvent.VK_V);
        Keys.map(W,                 KeyEvent.VK_W);
        Keys.map(X,                 KeyEvent.VK_X);
        Keys.map(Y,                 KeyEvent.VK_Y);
        Keys.map(Z,                 KeyEvent.VK_Z);

        Keys.map(F1,                KeyEvent.VK_F1);
        Keys.map(F2,                KeyEvent.VK_F2);
        Keys.map(F3,                KeyEvent.VK_F3);
        Keys.map(F4,                KeyEvent.VK_F4);
        Keys.map(F5,                KeyEvent.VK_F5);
        Keys.map(F6,                KeyEvent.VK_F6);
        Keys.map(F7,                KeyEvent.VK_F7);
        Keys.map(F8,                KeyEvent.VK_F8);
        Keys.map(F9,                KeyEvent.VK_F9);
        Keys.map(F10,               KeyEvent.VK_F10);
        Keys.map(F11,               KeyEvent.VK_F11);
        Keys.map(F12,               KeyEvent.VK_F12);
        Keys.map(F13,               KeyEvent.VK_F13);
        Keys.map(F14,               KeyEvent.VK_F14);
        Keys.map(F15,               KeyEvent.VK_F15);
        Keys.map(F16,               KeyEvent.VK_F16);
        Keys.map(F17,               KeyEvent.VK_F17);
        Keys.map(F18,               KeyEvent.VK_F18);
        Keys.map(F19,               KeyEvent.VK_F19);
        Keys.map(F20,               KeyEvent.VK_F20);
        Keys.map(F21,               KeyEvent.VK_F21);
        Keys.map(F22,               KeyEvent.VK_F22);
        Keys.map(F23,               KeyEvent.VK_F23);
        Keys.map(F24,               KeyEvent.VK_F24);
    }
}
