package com.offtime.flux.swing.window;

import com.offtime.flux.core.event.EventSystem;
import com.offtime.flux.core.event.mouse.MouseEvent;
import com.offtime.flux.core.input.MouseButton;
import com.offtime.flux.core.input.MouseState;

import javax.swing.SwingUtilities;
import java.awt.event.MouseAdapter;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class MouseListener extends MouseAdapter
{
    protected Window window;

    protected int x, y;
    protected int px, py;
    protected int rx, ry;

    public MouseListener(Window window)
    {
        this.window = window;
    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent e)
    {
        this.px = this.window.mouseX(e.getX());
        this.py = this.window.mouseY(e.getY());

        EventSystem.delayEvent("mouse", new MouseEvent(MouseState.PRESSED, captureMouseButton(e), this.px, this.py, this.px, this.py, this.px, this.py));
    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent e)
    {
        this.rx = this.window.mouseX(e.getX());
        this.ry = this.window.mouseY(e.getY());

        if (this.px != this.rx || this.py != this.ry)
            EventSystem.delayEvent("mouse", new MouseEvent(MouseState.DRAGGED, captureMouseButton(e), this.rx, this.ry, this.px, this.py, this.rx, this.ry));

        EventSystem.delayEvent("mouse", new MouseEvent(MouseState.RELEASED, captureMouseButton(e), this.rx, this.ry, this.rx, this.ry, this.rx, this.ry));
    }

    @Override
    public void mouseMoved(java.awt.event.MouseEvent e)
    {
        this.x = this.window.mouseX(e.getX());
        this.y = this.window.mouseY(e.getY());

        EventSystem.delayEvent("mouse", new MouseEvent(MouseState.MOVED, MouseButton.UNDEFINED, this.x, this.y, this.x, this.y, this.x, this.y));
    }

    public void fireEvents()
    {
        EventSystem.dispatchDelayedEvents("mouse");
    }

    protected static MouseButton captureMouseButton(java.awt.event.MouseEvent e)
    {
        if (SwingUtilities.isLeftMouseButton(e))
            return MouseButton.PRIMARY;
        else if (SwingUtilities.isMiddleMouseButton(e))
            return MouseButton.MIDDLE;
        else if (SwingUtilities.isRightMouseButton(e))
            return MouseButton.SECONDARY;
        else
            return MouseButton.UNDEFINED;
    }
}
