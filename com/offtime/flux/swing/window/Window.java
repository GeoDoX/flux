package com.offtime.flux.swing.window;

import com.offtime.flux.core.event.EventSystem;
import com.offtime.flux.core.event.window.WindowMovedEvent;
import com.offtime.flux.core.event.window.WindowReadyEvent;
import com.offtime.flux.core.event.window.WindowResizedEvent;
import com.offtime.flux.core.event.window.WindowVisibilityEvent;
import com.offtime.flux.core.window.IRenderer;
import com.offtime.flux.core.window.IWindow;
import com.offtime.flux.swing.FluxGame;
import com.offtime.flux.swing.render.Renderer;

import javax.swing.JFrame;
import javax.swing.Timer;
import java.awt.event.*;

/**
 * Created by GeoDoX on 2017-12-15.
 */
public class Window implements IWindow
{
    protected JFrame frame;
    protected Canvas canvas;

    protected MouseListener mouseListener;
    protected KeyListener keyListener;

    protected boolean closeRequested;

    public Window(FluxGame game, String title, int width, int height)
    {
        this.frame = new JFrame(title);
        this.canvas = new Canvas(this, game, width, height);

        this.frame.add(this.canvas);
        //this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.frame.addWindowListener(windowAdapter());
        this.frame.addComponentListener(windowResizeAdapter());

        keyListener = new KeyListener();
        this.frame.addKeyListener(keyListener);

        mouseListener = new MouseListener(this);
        this.frame.addMouseListener(mouseListener);
        this.frame.addMouseMotionListener(mouseListener);

        this.canvas.setFocusTraversalKeysEnabled(false);
        this.canvas.requestFocusInWindow();

        this.frame.pack();
        this.frame.setLocationRelativeTo(null);

        this.closeRequested = false;
    }

    public Renderer renderer()
    {
        return this.canvas.renderer;
    }

    @Override
    public IWindow renderer(IRenderer renderer)
    {
        this.canvas.renderer = (Renderer) renderer;

        return this;
    }

    protected WindowAdapter windowAdapter()
    {
        IWindow window = this;

        return new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                requestClose();
            }

            @Override
            public void windowOpened(WindowEvent e)
            {
                EventSystem.dispatch(new WindowReadyEvent(window));
            }
        };
    }

    protected ComponentAdapter windowResizeAdapter()
    {
        Timer resizeRecalcTimer = new Timer(100, (event) ->
        {
            canvas.size(frame.getWidth() - frame.getInsets().left, frame.getHeight() - frame.getInsets().top);

            EventSystem.dispatch(new WindowResizedEvent(width(), height()));
        });

        resizeRecalcTimer.setRepeats(false);

        Timer movedRecalcTimer = new Timer(100, (event) -> EventSystem.dispatch(new WindowMovedEvent(x(), y())));
        movedRecalcTimer.setRepeats(false);

        return new ComponentAdapter()
        {
            @Override
            public void componentResized(ComponentEvent e)
            {
                if (resizeRecalcTimer.isRunning())
                    resizeRecalcTimer.restart();
                else
                    resizeRecalcTimer.start();

                canvas.notifyResize();
            }

            @Override
            public void componentMoved(ComponentEvent e)
            {
                if (movedRecalcTimer.isRunning())
                    movedRecalcTimer.restart();
                else
                    movedRecalcTimer.start();
            }
        };
    }

    protected int mouseX(int mouseX)
    {
        return mouseX - this.frame.getInsets().left;
    }

    protected int mouseY(int mouseY)
    {
        return mouseY - this.frame.getInsets().top;
    }

    @Override
    public void update()
    {
        keyListener.fireEvents();
        mouseListener.fireEvents();
    }

    @Override
    public void draw()
    {
        this.canvas.repaint();
    }

    @Override
    public void dispose()
    {
        frame.dispose();
    }

    @Override
    public String title()
    {
        return this.frame.getTitle();
    }

    @Override
    public IWindow title(String title)
    {
        this.frame.setTitle(title);

        return this;
    }

    @Override
    public boolean visible()
    {
        return this.frame.isVisible();
    }

    @Override
    public IWindow visible(boolean visible)
    {
        this.frame.setVisible(visible);

        EventSystem.dispatch(new WindowVisibilityEvent(WindowVisibilityEvent.State.get(visible)));

        return this;
    }

    @Override
    public boolean resizable()
    {
        return this.frame.isResizable();
    }

    @Override
    public IWindow resizeable(boolean resizable)
    {
        this.frame.setResizable(resizable);

        return this;
    }

    @Override
    public boolean closeRequested()
    {
        return this.closeRequested;
    }

    @Override
    public void requestClose()
    {
        this.closeRequested = true;
    }

    @Override
    public int x()
    {
        return this.frame.getX();
    }

    @Override
    public IWindow x(int x)
    {
        this.frame.setLocation(x, y());

        return this;
    }

    @Override
    public int y()
    {
        return this.frame.getY();
    }

    @Override
    public IWindow y(int y)
    {
        this.frame.setLocation(x(), y);

        return this;
    }

    @Override
    public int width()
    {
        return this.canvas.getWidth();
    }

    @Override
    public IWindow width(int width)
    {
        this.canvas.size(width, height());

        return this;
    }

    @Override
    public int height()
    {
        return this.canvas.getHeight();
    }

    @Override
    public IWindow height(int height)
    {
        this.canvas.size(width(), height);

        return this;
    }
}
