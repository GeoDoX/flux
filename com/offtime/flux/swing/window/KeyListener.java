package com.offtime.flux.swing.window;

import com.offtime.flux.core.event.EventSystem;
import com.offtime.flux.core.input.KeyState;
import com.offtime.flux.core.input.Keys;

import java.awt.event.KeyAdapter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class KeyListener extends KeyAdapter
{
    private Set<Keys.Key> lastKeys = new HashSet<>(0);
    private Set<Keys.Key> nowKeys = new HashSet<>(0);

    private Set<Keys.Key> releasedKeys = new HashSet<>(0);

    private Iterator<Keys.Key> iterator;

    @Override
    public void keyPressed(java.awt.event.KeyEvent e)
    {
        int keyCode = e.getKeyCode();

        if (keyCode == java.awt.event.KeyEvent.VK_CONTROL   ||
            keyCode == java.awt.event.KeyEvent.VK_SHIFT     ||
            keyCode == java.awt.event.KeyEvent.VK_ALT)
            return;

        pressed(keyCode, e.isControlDown(), e.isShiftDown(), e.isAltDown());
    }

    @Override
    public void keyReleased(java.awt.event.KeyEvent e)
    {
        int keyCode = e.getKeyCode();

        if (keyCode == java.awt.event.KeyEvent.VK_CONTROL   ||
            keyCode == java.awt.event.KeyEvent.VK_SHIFT     ||
            keyCode == java.awt.event.KeyEvent.VK_ALT)
            return;

        released(keyCode, e.isControlDown(), e.isShiftDown(), e.isAltDown());
    }

    public void fireEvents()
    {
        // Pressed
        for (Keys.Key key : lastKeys)
            EventSystem.dispatch(new com.offtime.flux.core.event.key.KeyEvent(KeyState.PRESSED, key));

        // Released
        for (Keys.Key key : releasedKeys)
            EventSystem.dispatch(new com.offtime.flux.core.event.key.KeyEvent(KeyState.RELEASED, key));

        for (Keys.Key key : nowKeys)
        {
            if (!releasedKeys.contains(key))
                lastKeys.add(key);
        }

        iterator = lastKeys.iterator();

        while (iterator.hasNext())
        {
            Keys.Key key = iterator.next();

            if (releasedKeys.contains(key))
                iterator.remove();
        }

        nowKeys.clear();
        releasedKeys.clear();
    }

    private void pressed(Object keyCode, boolean controlKey, boolean shiftKey, boolean altKey)
    {
        Keys.Key key = Keys.fromKeyCode(keyCode);

        key.control = controlKey;
        key.shift = shiftKey;
        key.alt = altKey;

        nowKeys.add(key);
    }

    private void released(Object keyCode, boolean controlKey, boolean shiftKey, boolean altKey)
    {
        Keys.Key key = Keys.fromKeyCode(keyCode);

        key.control = controlKey;
        key.shift = shiftKey;
        key.alt = altKey;

        releasedKeys.add(key);
    }
}
