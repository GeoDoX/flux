package com.offtime.flux.swing.window;

import com.offtime.flux.core.window.IWindow;
import com.offtime.flux.swing.FluxGame;
import com.offtime.flux.swing.render.Renderer;

import javax.swing.JPanel;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class Canvas extends JPanel
{
    protected final FluxGame game;
    protected Renderer renderer;

    protected BufferedImage buffer;
    protected Graphics2D bufferGraphics;

    protected boolean resizing;

    protected Canvas(IWindow window, FluxGame game, int width, int height)
    {
        this.game = game;
        this.renderer = new Renderer(window);

        this.buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.bufferGraphics = this.buffer.createGraphics();

        this.renderer.context = this.bufferGraphics;

        setPreferredSize(new Dimension(width, height));
        setIgnoreRepaint(true);
    }

    protected void notifyResize()
    {
        this.resizing = true;
    }

    protected void size(int width, int height)
    {
        this.buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        this.setPreferredSize(new Dimension(width, height));

        this.resizing = false;
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        if (this.resizing)
            return;

        Graphics2D g2d = (Graphics2D) g;

        if (this.game != null)
        {
            if (game.properties().shouldUseAntiAliasing)
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            if (game.properties().shouldUseTextAntiAliasing)
                g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            if (game.properties().shouldUseQualityRendering)
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

            if (game.properties().shouldUseQualityColorRendering)
                g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);

            if (game.properties().shouldUseDithering)
                g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        }

        if (this.bufferGraphics != null)
        {
            g2d.drawImage(this.buffer, 0, 0, null);

            this.bufferGraphics.dispose();
        }

        this.bufferGraphics = this.buffer.createGraphics();

        this.renderer.context = this.bufferGraphics;
    }

    @Override
    public void update(Graphics g)
    {
        // Prevent Default Behaviour
    }
}
