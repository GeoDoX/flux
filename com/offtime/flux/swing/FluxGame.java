package com.offtime.flux.swing;

import com.offtime.flux.core.FluxCore;
import com.offtime.flux.core.event.EventSystem;
import com.offtime.flux.core.event.game.GameReadyEvent;
import com.offtime.flux.core.event.game.InitializationEvent;
import com.offtime.flux.core.event.game.PostInitializationEvent;
import com.offtime.flux.core.event.game.PreInitializationEvent;
import com.offtime.flux.core.event.mouse.MouseEvent;
import com.offtime.flux.core.input.Mouse;
import com.offtime.flux.core.mod.ModLoader;
import com.offtime.flux.core.mod.ModRef;
import com.offtime.flux.core.mod.ModRegistry;
import com.offtime.flux.core.util.logging.ILogger;
import com.offtime.flux.core.util.time.TimeUtils;
import com.offtime.flux.swing.input.SwingKeys;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

/**
 * Created by GeoDoX on 2017-12-15.
 */
public abstract class FluxGame extends FluxCore
{
    public FluxGame()
    {
        super(new Properties());
    }

    public FluxGame(Properties properties)
    {
        super(properties);
    }

    public FluxGame(Properties properties, ILogger logger)
    {
        super(properties, logger);
    }

    @Override
    public void _preInitialize()
    {
        super._preInitialize();

        EventSystem.register(new Mouse(), MouseEvent.class);
    }

    @Override
    public void start()
    {
        _preInitialize();
        _initialize();
        _postInitialize();

        // Map Keys to Swing Impl.
        new SwingKeys().map();

        this.deltaTime = 0;

        int targetFPS = properties.targetFPS;
        int targetUPS = properties.targetUPS;

        if (!properties().limitFPS)
            targetFPS = properties.maxFPS;

        logger.log(LOGGER_TAG, "Game Ready");

        EventSystem.dispatch(new GameReadyEvent(this));

        logger.log(LOGGER_TAG, "Starting Game Loop");

        long dUAccumulator = 0;
        long dFAccumulator = 0;
        long delta;
        long nsU = 1000 / targetUPS;
        long nsF = 1000 / targetFPS;
        long timer = System.currentTimeMillis();
        int updates = 0;
        int frames = 0;

        long time;
        long timeLast = System.currentTimeMillis();

        while (running())
        {
            time = System.currentTimeMillis();
            delta = time - timeLast;
            dUAccumulator += delta;
            dFAccumulator += delta;
            timeLast = time;

            while (dUAccumulator >= nsU)
            {
                this.deltaTime = dUAccumulator;

                update();
                dUAccumulator -= nsU;
                updates++;

                /*if (deltaTime >= 20)
                {
                    if (!didSpike)
                    {
                        didSpike = true;
                        System.out.println("SPIKE!");
                    }

                    System.out.println(deltaTime);
                }
                else
                    didSpike = false;*/
            }

            if (dFAccumulator >= nsF)
            {
                dFAccumulator -= nsF;
                render();
                frames++;
            }

            if (System.currentTimeMillis() - timer >= TimeUtils.second)
            {
                if (properties.printFPS_UPS)
                    System.out.println("Updates: " + updates + ", Frames: " + frames);

                timer += 1000;
                updates = 0;
                frames = 0;
            }
        }

        logger.log(LOGGER_TAG, "Disposing...");

        dispose();

        logger.log(LOGGER_TAG, "Disposing Mods...");

        for (ModRef ref : ModRegistry.enabled())
            ref.mod.unload();

        logger.log(LOGGER_TAG, "Disposing Mods... Done");

        EventSystem.dispose();

        logger.log(LOGGER_TAG, "Disposing ... Done");
        logger.log(LOGGER_TAG, "Closing");
    }

    @Override
    public Properties properties()
    {
        return (Properties) properties;
    }

    public static class Properties extends FluxCore.Properties
    {
        public boolean          shouldUseAntiAliasing = true;
        public boolean      shouldUseTextAntiAliasing = true;
        public boolean      shouldUseQualityRendering = true;
        public boolean shouldUseQualityColorRendering = true;
        public boolean             shouldUseDithering = true;

        @Override
        public Properties copy()
        {
            Properties copy = new Properties();

            copy.title   = this.title;
            copy.author  = this.author;
            copy.version = this.version;
            copy.build   = this.build;

            copy.fullscreen = this.fullscreen;
            copy.resizable  = this.resizable;

            copy.printFPS_UPS = this.printFPS_UPS;
            copy.limitFPS     = this.limitFPS;
            copy.vsync        = this.vsync;
            copy.targetFPS    = this.targetFPS;
            copy.targetUPS    = this.targetUPS;

            copy.printLog      = this.printLog;
            copy.saveLog       = this.saveLog;
            copy.deleteOldLogs = this.deleteOldLogs;
            copy.keptLogs      = this.keptLogs;

            copy.shouldUseAntiAliasing          = this.shouldUseAntiAliasing;
            copy.shouldUseTextAntiAliasing      = this.shouldUseTextAntiAliasing;
            copy.shouldUseQualityRendering      = this.shouldUseQualityRendering;
            copy.shouldUseQualityColorRendering = this.shouldUseQualityColorRendering;
            copy.shouldUseDithering             = this.shouldUseDithering;

            return copy;
        }
    }
}
