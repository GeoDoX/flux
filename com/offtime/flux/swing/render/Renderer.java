package com.offtime.flux.swing.render;

import com.offtime.flux.core.window.IRenderer;
import com.offtime.flux.core.window.IWindow;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 * Created by GeoDoX on 2018-12-18.
 */
public class Renderer implements IRenderer
{
    public final IWindow window;

    public Graphics2D context;
    public Color clearColor;

    private AffineTransform pushedTransform;

    public Renderer(IWindow window)
    {
        this.window = window;
    }

    @Override
    public IWindow window()
    {
        return window;
    }

    @Override
    public void clear(int x, int y, int width, int height)
    {
        clear(x, y, width, height, clearColor);
    }

    @Override
    public void clear(int x, int y, int width, int height, Color color)
    {
        if (this.context != null)
        {
            this.context.setColor(color);
            this.context.fillRect(x, y, width, height);
        }
    }

    @Override
    public void push()
    {
        if (this.context == null)
            return;

        this.pushedTransform = this.context.getTransform();
    }

    @Override
    public void translate(int x, int y)
    {
        if (this.context == null)
            return;

        this.context.translate(x, y);
    }

    @Override
    public void scale(float x, float y)
    {
        if (this.context == null)
            return;

        this.context.scale(x, y);
    }

    @Override
    public void pop()
    {
        if (this.context == null)
            return;

        this.context.setTransform(this.pushedTransform);
        this.pushedTransform = null;
    }
}
