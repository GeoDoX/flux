package com.offtime.flux.core;

import com.offtime.flux.core.event.EventSystem;
import com.offtime.flux.core.event.game.GameReadyEvent;
import com.offtime.flux.core.event.game.InitializationEvent;
import com.offtime.flux.core.event.game.PostInitializationEvent;
import com.offtime.flux.core.event.game.PreInitializationEvent;
import com.offtime.flux.core.event.mouse.MouseEvent;
import com.offtime.flux.core.files.Directory;
import com.offtime.flux.core.input.Mouse;
import com.offtime.flux.core.mod.ModLoader;
import com.offtime.flux.core.mod.ModRef;
import com.offtime.flux.core.mod.ModRegistry;
import com.offtime.flux.core.util.logging.ILogger;
import com.offtime.flux.core.util.logging.LoggerTag;
import com.offtime.flux.core.util.logging.impl.Logger;
import com.offtime.flux.core.util.time.TimeUtils;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public abstract class FluxCore
{
    public static final LoggerTag LOGGER_TAG = new LoggerTag("Flux.Core");

    protected Properties properties;
    protected ILogger logger;

    public long deltaTime;
    public boolean didSpike;

    public FluxCore()
    {
        this(new Properties());
    }

    public FluxCore(Properties properties)
    {
        this.properties = Objects.requireNonNull(properties, "Null Properties");
        this.logger = Objects.requireNonNull(new Logger(this, System.out), "Null Logger");
    }

    public FluxCore(Properties properties, ILogger logger)
    {
        this.properties = Objects.requireNonNull(properties, "Null Properties");
        this.logger = Objects.requireNonNull(logger, "Null Logger");
    }

    public void _preInitialize()
    {
        logger.log(LOGGER_TAG, "Initializing Flux ...");

        EventSystem.logger(logger);
    }

    public void _initialize()
    {
        try
        {
            logger.log(LOGGER_TAG, "Loading Mods ...");

            // Registering Mods
            ModLoader.load(modsDirectory());

            // Loading Mods
            for (ModRef ref : ModRegistry.registered())
            {
                ref.mod.game = this;
                ref.mod.load();
            }
        }
        catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e)
        {
            if (logger == null)
                e.printStackTrace();
            else
            {
                logger.log(LOGGER_TAG, "Exception thrown while attempting to load Mods");
                logger.log(LOGGER_TAG, e);
            }
        }
        logger.log(LOGGER_TAG, "Loading Mods ... Done");

        initialize();

        logger.log(LOGGER_TAG, "Initializing Flux ... Done");
    }

    public void _postInitialize()
    {
        logger.log(LOGGER_TAG, "PreInitializing ...");
        EventSystem.dispatch(new PreInitializationEvent());
        logger.log(LOGGER_TAG, "PreInitializing ... Done");

        logger.log(LOGGER_TAG, "Initializing ...");
        EventSystem.dispatch(new InitializationEvent());
        logger.log(LOGGER_TAG, "Initializing ... Done");

        logger.log(LOGGER_TAG, "PostInitializing ...");
        EventSystem.dispatch(new PostInitializationEvent());
        logger.log(LOGGER_TAG, "PostInitializing ... Done");
    }

    public abstract void start();

    public float deltaTime()
    {
        return this.deltaTime;
    }

    public abstract void initialize();
    public abstract void update();
    public abstract void render();
    public abstract void dispose();

    public abstract boolean running();

    public abstract Directory modsDirectory();

    public Properties properties()
    {
        return this.properties.copy();
    }

    public ILogger logger()
    {
        return this.logger;
    }

    public static class Properties implements IProperties
    {
        public String   title = "Flux Game";
        public String  author = "";
        public String version = "0.0.0";
        public String   build = "0";

        public boolean fullscreen = false;
        public boolean  resizable = true;

        public boolean printFPS_UPS = false;
        public boolean     limitFPS = true;
        public boolean        vsync = false;
        public int        targetFPS = 60;
        public int        targetUPS = 60;
        public int           maxFPS = 287; // ;) 'tis a secret

        public boolean      printLog = true;
        public boolean       saveLog = false;
        public boolean deleteOldLogs = true;
        public int          keptLogs = 5;

        @Override
        public Properties copy()
        {
            Properties copy = new Properties();

            copy.title   = this.title;
            copy.author  = this.author;
            copy.version = this.version;
            copy.build   = this.build;

            copy.fullscreen = this.fullscreen;
            copy.resizable  = this.resizable;

            copy.printFPS_UPS = this.printFPS_UPS;
            copy.limitFPS     = this.limitFPS;
            copy.vsync        = this.vsync;
            copy.targetFPS    = this.targetFPS;
            copy.targetUPS    = this.targetUPS;

            copy.printLog      = this.printLog;
            copy.saveLog       = this.saveLog;
            copy.deleteOldLogs = this.deleteOldLogs;
            copy.keptLogs      = this.keptLogs;

            return copy;
        }
    }
}
