package com.offtime.flux.core.mod;

import java.util.Objects;

/**
 * Created by GeoDoX on 2019-01-29.
 */
public class ModRef
{
    public String id;
    public Mod mod;

    public boolean enabled;

    public ModRef(Mod mod)
    {
        this.mod = Objects.requireNonNull(mod, "Mod Reference is null.");
        this.id = mod.config.id;

        this.enabled = mod.config.get("enabled").toLowerCase().equals("true");
    }
}
