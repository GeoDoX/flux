package com.offtime.flux.core.mod;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by GeoDoX on 2019-01-29.
 */
public class ModRegistry
{
    private static Set<ModRef> mods = new HashSet<>(0);

    public static void register(ModRef ref)
    {
        if (ref != null)
            mods.add(ref);
    }

    public static void unregister(ModRef ref)
    {
        if (ref != null)
            mods.remove(ref);
    }

    public static Set<ModRef> enabled()
    {
        Set<ModRef> enabled = new HashSet<>(0);

        for (ModRef ref : mods)
        {
            if (ref.enabled)
                enabled.add(ref);
        }

        return enabled;
    }

    public static Set<ModRef> disabled()
    {
        Set<ModRef> disabled = new HashSet<>(0);

        for (ModRef ref : mods)
        {
            if (!ref.enabled)
                disabled.add(ref);
        }

        return disabled;
    }

    public static Set<ModRef> registered()
    {
        return mods;
    }

    public static boolean isRegistered(String modID)
    {
        for (ModRef ref : mods)
        {
            if (ref.id.equals(modID))
                return true;
        }

        return false;
    }

    public static boolean isEnabled(String modID)
    {
        for (ModRef ref : enabled())
        {
            if (ref.id.equals(modID))
                return ref.enabled;
        }

        return false;
    }
}
