package com.offtime.flux.core.mod;

import com.offtime.flux.core.files.Directory;
import com.offtime.flux.core.files.File;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarFile;

/**
 * Created by GeoDoX on 2019-01-23.
 */
public class ModLoader
{
    public static Class<?>[] load(String file) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        File f = new File(file);

        if (f.isFile())
            load(f);
        else if (f.isDirectory())
            load((Directory) f);

        return new Class<?>[0];
    }

    public static void load(File file) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        file = Objects.requireNonNull(file, "File is null.");

        if (!file.exists())
            return;

        if (file.isDirectory())
            load((Directory) file);

        LinkedHashMap<String, String> data;

        try
        (
            JarFile jar = new JarFile(file.asFile());
            InputStreamReader isr = new InputStreamReader(jar.getInputStream(jar.getJarEntry("config.cfg")));
            BufferedReader r = new BufferedReader(isr);
        )
        {
            data = r.lines()
                    .map(str -> str.split("\\s*=\\s*"))
                    .collect(LinkedHashMap::new, (map, str) -> map.put(str[0], str[1].replaceAll("\"", "")), Map::putAll);
        }

        // Create and Register Mod
        ModRegistry.register(createInstance(file, data));
    }

    public static void load(Directory dir) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        if (dir == null || !dir.exists())
            return;

        if (dir.isFile())
            load((File) dir);

        for (File file : dir.files())
            load(file);
    }

    private static ModRef createInstance(File file, LinkedHashMap<String, String> data) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Mod mod = (Mod) Class.forName(data.get("entry"), true, new URLClassLoader(new URL[]{file.asPath().toUri().toURL()})).newInstance();
        mod.config = new ModConfig(file, data);

        return new ModRef(mod);
    }
}
