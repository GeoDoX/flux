package com.offtime.flux.core.mod;

import com.offtime.flux.core.files.File;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by GeoDoX on 2019-01-23.
 */
public class ModConfig
{
    private final File cfgFile;
    private final LinkedHashMap<String, String> data;

    public final String id;
    public final String name;
    public final String version;
    public final String author;

    public ModConfig(File cfgFile, LinkedHashMap<String, String> data)
    {
        this.cfgFile = cfgFile;
        this.data = data;

        this.id = get("id");
        this.name = get("name");
        this.version = get("version");
        this.author = get("author");
    }

    public String get(String key)
    {
        return data.get(key);
    }

    public void write()
    {
        try (FileWriter w = new FileWriter(cfgFile.asFile()))
        {
            for (Map.Entry<String, String> dataEntry : data.entrySet())
                w.write(dataEntry.getKey() + " = " + dataEntry.getValue());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
