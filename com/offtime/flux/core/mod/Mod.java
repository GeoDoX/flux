package com.offtime.flux.core.mod;

import com.offtime.flux.core.FluxCore;

/**
 * Created by GeoDoX on 2019-01-23.
 */
public abstract class Mod
{
    public FluxCore game;
    public ModConfig config;

    public abstract void load();
    public abstract void unload();
}
