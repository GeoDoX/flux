package com.offtime.flux.core.gamestate;

import com.offtime.flux.core.FluxCore;
import com.offtime.flux.core.window.IRenderer;

import java.util.Objects;
import java.util.Stack;

/**
 * Created by GeoDoX on 2017-12-16.
 */
public class GameStateManager
{
    public final FluxCore game;

    protected Stack<GameState> stateStack;

    protected int passthrough;
    protected boolean updatePassthrough;
    protected boolean renderPassthrough;

    public GameStateManager(FluxCore game)
    {
        this.game = game;
        this.stateStack = new Stack<>();
    }

    public void push(GameState state)
    {
        state = Objects.requireNonNull(state, "Null State");

        deactivate();

        stateStack.push(state);

        activate();
    }

    public GameState peek()
    {
        if (!empty())
            return stateStack.peek();

        return null;
    }

    public GameState peek(int passthrough)
    {
        if (passthrough < stateStack.size())
            return stateStack.get(stateStack.size() - (passthrough + 1));

        return null;
    }

    public void pop()
    {
        deactivate();

        if (!empty())
            stateStack.pop();

        activate();
    }

    public void set(GameState state)
    {
        pop();
        push(state);
    }

    public boolean empty()
    {
        return stateStack.empty();
    }

    public void update(long dt)
    {
        passthrough = 0;

        do
        {
            if (!empty())
            {
                updatePassthrough = peek(passthrough).update(dt);
                passthrough++;
            }
            else
                break;
        }
        while (updatePassthrough);
    }

    public void render(IRenderer renderer)
    {
        passthrough = 0;

        do
        {
            if (!empty())
            {
                renderPassthrough = peek(passthrough).render(renderer);
                passthrough++;
            }
            else
                break;
        }
        while (renderPassthrough);
    }

    public void dispose()
    {
        while (!empty())
            pop();
    }

    public boolean isActive(GameState state)
    {
        return this.peek() == state;
    }

    private void deactivate()
    {
        if (!empty())
            peek().onInactive();
    }

    private void activate()
    {
        if (!empty())
            peek().onActive();
    }
}
