package com.offtime.flux.core.gamestate;

import com.offtime.flux.core.window.IRenderer;

import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-16.
 */
public abstract class GameState
{
    public final GameStateManager gsm;

    public GameState(GameStateManager gsm)
    {
        this.gsm = Objects.requireNonNull(gsm, "Null StateManager");

        onInitialized();
    }

    public void onInitialized() {}
    public void onActive() {}
    public void onInactive() {}

    public boolean update(long dt)
    {
        return false;
    }

    public boolean render(IRenderer renderer)
    {
        return false;
    }

    public boolean isActive()
    {
        return this.gsm.isActive(this);
    }
}
