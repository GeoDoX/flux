package com.offtime.flux.core.util;

/**
 * Created by GeoDoX on 2018-12-23.
 */
public class Property<T>
{
    private T value;
    private Getter<T> getter;
    private Setter<T> setter;

    public Property(Getter<T> getter, Setter<T> setter)
    {
        this.getter = getter;
        this.setter = setter;
    }

    public T get()
    {
        if (getter != null)
            this.getter.get(value);

        return value;
    }

    public void set(T value)
    {
        if (setter != null)
            this.setter.set(value);

        this.value = value;
    }

    public interface Setter<T>
    {
        void set(T value);
    }

    public interface Getter<T>
    {
        void get(T value);
    }
}
