package com.offtime.flux.core.util;

/**
 * Created by GeoDoX on 2018-12-23.
 */
public class GeneralUtils
{
    public static boolean rectContains(int rx, int ry, int rw, int rh, int px, int py)
    {
        return (px >= rx && px <= rx + rw && py >= ry && py <= ry + rh);
    }

    public static boolean rectContains(float rx, float ry, float rw, float rh, float px, float py)
    {
        return (px >= rx && px <= rx + rw && py >= ry && py <= ry + rh);
    }

    public static int constrain(int value, int min, int max)
    {
        return Math.min(Math.max(value, min), max);
    }
}
