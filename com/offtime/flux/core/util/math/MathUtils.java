package com.offtime.flux.core.util.math;

/**
 * Created by GeoDoX on 2017-12-15.
 */
public class MathUtils
{
    public static double det(double a, double b, double c, double d)
    {
        return a * d - b * c;
    }

    public static float clamp(float v, float min, float max)
    {
        return Math.max(min, Math.min(max, v));
    }
}
