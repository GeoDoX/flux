package com.offtime.flux.core.util;

/**
 * Created by GeoDoX on 2016-10-21.
 */
public interface IDelegate
{
    void invoke();
}
