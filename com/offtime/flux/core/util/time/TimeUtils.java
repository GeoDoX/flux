package com.offtime.flux.core.util.time;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public class TimeUtils
{
    public static long      second = 1;
    public static long millisecond = 1000;
    public static long  nanosecond = 1000000000;

    public static long calculateDeltaTime(long dt)
    {
        return dt * nanosecond;
    }

    public static String dateString(LocalDateTime dateTime)
    {
        dateTime = Objects.requireNonNull(dateTime, "Null DateTime");

        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(dateTime);
    }

    public static String fileSafeDateString(LocalDateTime dateTime)
    {
        dateTime = Objects.requireNonNull(dateTime, "Null DateTime");

        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss").format(dateTime);
    }
}
