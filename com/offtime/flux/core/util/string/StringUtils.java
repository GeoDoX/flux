package com.offtime.flux.core.util.string;

import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-12.
 */
public class StringUtils
{
    public static String substring(String string, int endIndex)
    {
        string = Objects.requireNonNull(string, "Null String");

        return string.substring(0, endIndex);
    }

    public static boolean nullOrEmpty(String s)
    {
        return s == null || s.isEmpty();
    }
}
