package com.offtime.flux.core.util.color;

import com.offtime.flux.core.util.random.RandomUtils;

import java.awt.Color;
import java.util.Objects;
import java.util.Random;

/**
 * Created by GeoDoX on 2016-09-26.
 */
public abstract class Colors
{
    public static final Color TRANSPARENT = fromHex(0x00000000);

    public static final Color WHITE = new Color(0xffffff);
    public static final Color LIGHT_GRAY = new Color(0xbfbfbf);
    public static final Color GRAY = new Color(0x7f7f7f);
    public static final Color DARK_GRAY = new Color(0x3f3f3f);
    public static final Color BLACK = fromHex(0x000000ff);

    public static final Color BLUE = new Color(0x0000FF);
    public static final Color NAVY = new Color(0x00007F);
    public static final Color ROYAL = new Color(0x4169e1);
    public static final Color SLATE = new Color(0x708090);
    public static final Color SKY = new Color(0x87ceeb);
    public static final Color CYAN = new Color(0x00FFFF);
    public static final Color TEAL = new Color(0x007F7F);

    public static final Color GREEN = new Color(0x00ff00);
    public static final Color CHARTREUSE = new Color(0x7fff00);
    public static final Color LIME = new Color(0x32cd32);
    public static final Color FOREST = new Color(0x228b22);
    public static final Color OLIVE = new Color(0x6b8e23);

    public static final Color YELLOW = new Color(0xffff00);
    public static final Color GOLD = new Color(0xffd700);
    public static final Color GOLDENROD = new Color(0xdaa520);
    public static final Color ORANGE = new Color(0xffa500);

    public static final Color BROWN = new Color(0x8b4513);
    public static final Color TAN = new Color(0xd2b48c);
    public static final Color FIREBRICK = new Color(0xb22222);

    public static final Color RED = new Color(0xff0000);
    public static final Color SCARLET = new Color(0xff341c);
    public static final Color CORAL = new Color(0xff7f50);
    public static final Color SALMON = new Color(0xfa8072);
    public static final Color PINK = new Color(0xff69b4);
    public static final Color MAGENTA = new Color(0xFF00FF);

    public static final Color PURPLE = new Color(0xa020f0);
    public static final Color VIOLET = new Color(0xee82ee);
    public static final Color MAROON = new Color(0xb03060);

    public static Color fromHex(int hexCode)
    {
        int r = (hexCode & 0xFF000000) >>> 24;
        int g = (hexCode & 0x00FF0000) >> 16;
        int b = (hexCode & 0x0000FF00) >> 8;
        int a = (hexCode & 0x000000FF);

        return fromRGB(r, g, b, a);
    }

    public static Color fromRGB(int r, int g, int b)
    {
        return new Color(r, g, b, 255);
    }

    public static Color fromRGB(int r, int g, int b, int a)
    {
        return new Color(r, g, b, a);
    }

    public static Color withAlpha(Color color, int a)
    {
        color = Objects.requireNonNull(color, "Null Color");

        return new Color(color.getRed(), color.getGreen(), color.getBlue(), a);
    }

    public static Color random(Random random, int a)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return fromRGB(RandomUtils.nextInt(random, 0, 255), RandomUtils.nextInt(random, 0, 255), RandomUtils.nextInt(random, 0, 255), a);
    }

    public static Color blend(Color c1, Color c2)
    {
        c1 = Objects.requireNonNull(c1, "Null Color");
        c2 = Objects.requireNonNull(c2, "Null Color");

        double totalAlpha = c1.getAlpha() + c2.getAlpha();
        double weight0 = c1.getAlpha() / totalAlpha;
        double weight1 = c2.getAlpha() / totalAlpha;

        double r = weight0 * c1.getRed() + weight1 * c2.getRed();
        double g = weight0 * c1.getGreen() + weight1 * c2.getGreen();
        double b = weight0 * c1.getBlue() + weight1 * c2.getBlue();
        double a = Math.max(c1.getAlpha(), c2.getAlpha());

        return new Color((int) r, (int) g, (int) b, (int) a);
    }
}
