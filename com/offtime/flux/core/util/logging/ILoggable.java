package com.offtime.flux.core.util.logging;

/**
 * Created by GeoDoX on 2017-12-13.
 */
public interface ILoggable
{
    String logString();
}
