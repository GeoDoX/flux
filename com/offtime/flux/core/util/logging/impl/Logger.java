package com.offtime.flux.core.util.logging.impl;

import com.offtime.flux.core.FluxCore;
import com.offtime.flux.core.files.Files;
import com.offtime.flux.core.files.File;
import com.offtime.flux.core.files.Directory;
import com.offtime.flux.core.util.logging.ILoggable;
import com.offtime.flux.core.util.logging.ILogger;
import com.offtime.flux.core.util.logging.LoggerTag;
import com.offtime.flux.core.util.time.TimeUtils;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.io.*;
import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-13.
 */
public final class Logger implements ILogger
{
    private FluxCore game;

    // The PrintStream to print to
    private PrintStream printStream;

    // The lines that have been logged
    private List<String> logList = new ArrayList<>(0);

    // The StringWriter and PrintWriter used to convert Exceptions to Strings
    private static final StringWriter exceptionString = new StringWriter();
    private static final PrintWriter exceptionWriter = new PrintWriter(exceptionString);

    public Logger(FluxCore game)
    {
        this(game, System.out);
    }

    public Logger(FluxCore game, PrintStream printStream)
    {
        this.game = game;
        this.printStream = Objects.requireNonNull(printStream, "Null PrintStream");
    }

    @Override
    public void log(LoggerTag tag, String message)
    {
        tag = Objects.requireNonNull(tag, "Null Tag");
        message = Objects.requireNonNull(message, "Null Message");

        logPrintLine(tag.toString() + ": " + message);
    }

    @Override
    public void log(LoggerTag tag, Exception exception)
    {
        tag = Objects.requireNonNull(tag, "Null Tag");
        exception = Objects.requireNonNull(exception, "Null Exception");

        // Convert Exception to String, stored in exceptionString
        exception.printStackTrace(exceptionWriter);

        logPrintLine(tag.toString() + ": " + exceptionString.toString());
    }

    @Override
    public void log(LoggerTag tag, Object object)
    {
        tag = Objects.requireNonNull(tag, "Null Tag");
        object = Objects.requireNonNull(object, "Null Object");

        logPrintLine(tag.toString() + ": " + object.toString());
    }

    @Override
    public void log(LoggerTag tag, ILoggable loggable)
    {
        tag = Objects.requireNonNull(tag, "Null Tag");
        loggable = Objects.requireNonNull(loggable, "Null Loggable");

        logPrintLine(tag.toString() + ": " + loggable.logString());
    }

    @Override
    public void save()
    {
        if(!this.game.properties().saveLog)
            return;

        try
        {
            // Grab the Directory the logs are stored in
            Directory logsDirectory = new Directory(Files.path(Files.WORKING_DIRECTORY_PATH, "logs"));
            // Grab the File to write the log to
            File logFile = new com.offtime.flux.core.files.File(Files.path(logsDirectory.path(), "Log " + TimeUtils.fileSafeDateString(LocalDateTime.now()) + ".log"));

            // Create them if necessary
            logsDirectory.create();
            logFile.create();

            // Create the writer
            OutputStream fileStream = logFile.outputStream();
            OutputStreamWriter fileOS = new OutputStreamWriter(fileStream);

            // Write the lines respecting the Systems line separator
            for (String s : logList)
            {
                fileOS.write(s);
                fileOS.write(System.lineSeparator());
            }

            // Flush and Close the stream
            fileOS.flush();
            fileOS.close();

            // Delete old logs if necessary
            if (this.game.properties().deleteOldLogs)
                deleteOldLogs(logsDirectory, this.game.properties().keptLogs);
        }
        catch (IOException e)
        {
            e.printStackTrace(exceptionWriter);
            printStream.println(exceptionString.toString());
        }
    }

    private void logPrintLine(String line)
    {
        line = Objects.requireNonNull(line, "Null Line");

        // Print the line to the PrintStream
        if (this.game.properties().printLog)
            this.printStream.println(line);

        // Add the line to the logged lines
        logList.add(line);
    }

    private void deleteOldLogs(Directory logDirectory, int keptLogs) throws IOException
    {
        logDirectory = Objects.requireNonNull(logDirectory, "Null Directory");

        List<File> logFiles = new ArrayList<>(0);

        for (File logFile : logDirectory.files())
        {
            // If the file is a valid log file, add it to the list
            if (logFile.path().endsWith(".log"))
                logFiles.add(logFile);
        }

        // Sort the log files
        logFiles = sortedLogsNewestFirst(logFiles);

        // Delete the oldest
        for (int i = keptLogs; i < logFiles.size() - 1; i++)
            logFiles.get(i).delete();
    }

    private List<File> sortedLogsNewestFirst(List<File> logFiles)
    {
        logFiles = Objects.requireNonNull(logFiles, "Null Files");

        // TODO: Check if logs are being sorted properly
        List<File> sortedLogFiles = new ArrayList<>(0);

        if (logFiles == null)
            return sortedLogFiles;

        // Sort newest to oldest
        Collections.sort(sortedLogFiles, (logFile1, logFile2) -> Long.compare(logFile2.lastModified(), logFile1.lastModified()));

        return sortedLogFiles;
    }
}
