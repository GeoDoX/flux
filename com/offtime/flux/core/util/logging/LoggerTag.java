package com.offtime.flux.core.util.logging;

import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-13.
 */
public class LoggerTag
{
    public final String tag;

    public LoggerTag(String tag)
    {
        this.tag = Objects.requireNonNull(tag, "Null Tag");
    }

    @Override
    public String toString()
    {
        return "[" + tag + "]";
    }
}
