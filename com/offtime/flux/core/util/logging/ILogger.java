package com.offtime.flux.core.util.logging;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public interface ILogger
{
    void log(LoggerTag tag, String message);
    void log(LoggerTag tag, Exception exception);
    void log(LoggerTag tag, Object object);
    void log(LoggerTag tag, ILoggable loggable);

    void save();
}
