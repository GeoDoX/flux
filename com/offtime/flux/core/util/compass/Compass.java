package com.offtime.flux.core.util.compass;

/**
 * Created by GeoDoX on 2018-12-07.
 */
public abstract class Compass
{
    public enum Axis
    {
        VERTICAL,
        HORIZONTAL
    }

    public static class Direction
    {
        public enum Cardinal
        {
            NORTH,
            SOUTH,
            EAST,
            WEST,
        }

        public enum Ordinal
        {
            NORTHEAST,
            NORTHWEST,
            SOUTHEAST,
            SOUTHWEST
        }
    }

}
