package com.offtime.flux.core.util;

/**
 * Created by GeoDoX on 2018-01-19.
 */
public class Pair<L, R>
{
    public L first;
    public R second;

    public Pair(L first, R second)
    {
        this.first = first;
        this.second = second;
    }

    public void set(L first, R second)
    {
        this.first = first;
        this.second = second;
    }

    @SuppressWarnings("unchecked")
    public static <L, R> Pair<L, R>[] build(Object[][] tuples) throws ClassCastException
    {
        Pair<L, R>[] result = (Pair<L, R>[]) new Pair[tuples.length];

        for (int i = 0; i < result.length; i++)
            result[i] = new Pair<>((L) tuples[i][0], (R) tuples[i][1]);

        return result;
    }

    @SuppressWarnings("unchecked")
    public static <L, R> Pair<L, R>[] pairwiseConcat(L[] array0, R[] array1)
    {
        Pair<L, R>[] result = (Pair<L, R>[]) new Pair[Math.min(array0.length, array1.length)];

        for (int i = 0; i < result.length; i++)
            result[i] = new Pair<>(array0[i], array1[i]);

        return result;
    }
}
