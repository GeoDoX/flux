package com.offtime.flux.core.util.serialization;

import static com.offtime.flux.core.util.serialization.FSUtils.*;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public class FSField extends FSBase
{
    public final byte containerType = getContainerType(this);

    public byte primitiveType;
    public byte[] data;

    private FSField() {}

    public byte getByte() {
        return data[0];
    }

    public short getShort() {
        return readShort(data, 0);
    }

    public char getChar() {
        return readChar(data, 0);
    }

    public int getInt() {
        return readInt(data, 0);
    }

    public long getLong() {
        return readLong(data, 0);
    }

    public double getDouble() {
        return readDouble(data, 0);
    }

    public float getFloat() {
        return readFloat(data, 0);
    }

    public boolean getBoolean() {
        return readBoolean(data, 0);
    }

    public int getBytes(byte[] dest, int pointer)
    {
        pointer = writeBytes(dest, pointer, containerType);
        pointer = writeBytes(dest, pointer, nameLength);
        pointer = writeBytes(dest, pointer, name);
        pointer = writeBytes(dest, pointer, primitiveType);
        pointer = writeBytes(dest, pointer, data);
        return pointer;
    }

    @Override
    public int size()
    {
        // TODO: Analyze and Comment
        return 1 + 2 + name.length + 1 + data.length;
    }

    public static FSField Byte(String name, byte value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_BYTE;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_BYTE)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Short(String name, short value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_SHORT;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_SHORT)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Char(String name, char value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_CHAR;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_CHAR)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Integer(String name, int value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_INTEGER;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_INTEGER)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Long(String name, long value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_LONG;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_LONG)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Float(String name, float value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_FLOAT;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_FLOAT)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Double(String name, double value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_DOUBLE;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_DOUBLE)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Boolean(String name, boolean value)
    {
        FSField field = new FSField();
        field.name(name);
        field.primitiveType = PRIMITIVE_BOOLEAN;
        field.data = new byte[getPrimitiveSize(PRIMITIVE_BOOLEAN)];
        writeBytes(field.data, 0, value);
        return field;
    }

    public static FSField Deserialize(byte[] data, int pointer)
    {
        byte containerType = data[pointer++];

        FSField result = new FSField();
        result.nameLength = readShort(data, pointer);
        pointer += 2;
        result.name = readString(data, pointer, result.nameLength).getBytes();
        pointer += result.nameLength;

        result.primitiveType = data[pointer++];

        result.data = new byte[getPrimitiveSize(result.primitiveType)];
        readBytes(data, pointer, result.data);
        pointer += getPrimitiveSize(result.primitiveType);
        return result;
    }
}
