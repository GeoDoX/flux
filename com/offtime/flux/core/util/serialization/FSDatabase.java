package com.offtime.flux.core.util.serialization;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.offtime.flux.core.util.serialization.FSUtils.*;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public class FSDatabase extends FSBase
{
    /*
    * TODO:
    *   - Implement Versioning (see TheKeyMaster.SpriteDefinitionLoader)
    *   - Abstract, Abstract, Abstract
    *   - Interface to note an Object is Serializable
    *   - Implement toString() in each FSBase Subclass
    *   - FUCKING USE PRIMITIVE SIZES!!
    * */

    /*
    * Format:
    *   - Header
    *       - identifier
    *       - version
    *   - Data
    *   - Footer
    *       - Hash/Checksum of Object (String)
    * */

    public static final byte[] HEADER = "FSDB".getBytes();
    public static final short VERSION = 0x0100;

    public final byte containerType = getContainerType(this);

    public List<FSObject> objects = new ArrayList<>();

    private FSDatabase() {}

    public FSDatabase(String name)
    {
        this.name(name);
        this.size += HEADER.length + 2 + 1 + 2;
    }

    public void addObject(FSObject object)
    {
        objects.add(object);
        size += object.size();
    }

    public int getBytes(byte[] dest, int pointer)
    {
        pointer = writeBytes(dest, pointer, HEADER);
        pointer = writeBytes(dest, pointer, VERSION);
        pointer = writeBytes(dest, pointer, containerType);
        pointer = writeBytes(dest, pointer, nameLength);
        pointer = writeBytes(dest, pointer, name);
        pointer = writeBytes(dest, pointer, size);

        pointer = writeBytes(dest, pointer, objects.size());

        for (FSObject object : objects)
            pointer = object.getBytes(dest, pointer);

        return pointer;
    }

    public static FSDatabase Deserialize(byte[] data)
    {
        int pointer = 0;
        pointer += HEADER.length;

        if (readShort(data, pointer) != VERSION)
        {
            System.err.println("Invalid FSDB version!");
            return null;
        }

        pointer += 2;

        pointer++; // Nudge the ContainerType out of the way

        FSDatabase result = new FSDatabase();
        result.nameLength = readShort(data, pointer);
        pointer += 2;
        result.name = readString(data, pointer, result.nameLength).getBytes();
        pointer += result.nameLength;

        result.size = readInt(data, pointer);
        pointer += 4;

        int objectCount = readShort(data, pointer);
        pointer += 2;

        for (int i = 0; i < objectCount; i++)
        {
            FSObject object = FSObject.Deserialize(data, pointer);
            result.objects.add(object);
            pointer += object.size();
        }

        return result;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static FSDatabase DeserializeFromFile(String path)
    {
        byte[] buffer;

        try
        {
            BufferedInputStream stream = new BufferedInputStream(new FileInputStream(path));
            buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        return Deserialize(buffer);
    }

    public void serializeToFile(String path)
    {
        byte[] data = new byte[size()];

        getBytes(data, 0);

        try
        {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(path));
            stream.write(data);
            stream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
