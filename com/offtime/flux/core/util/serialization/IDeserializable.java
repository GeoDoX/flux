package com.offtime.flux.core.util.serialization;

/**
 * Created by GeoDoX on 2019-01-16.
 */
public interface IDeserializable<T>
{
    T deserialize(SerializationReader reader);
}
