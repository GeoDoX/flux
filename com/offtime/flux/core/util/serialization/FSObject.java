package com.offtime.flux.core.util.serialization;

import java.util.ArrayList;
import java.util.List;

import static com.offtime.flux.core.util.serialization.FSUtils.*;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public class FSObject extends FSBase
{
    public final byte containerType = getContainerType(this);

    // TODO: Look into using a Map (Example: Field, key = field.name(), value = field)
    public List<FSField> fields = new ArrayList<>();
    public List<FSString> strings = new ArrayList<>();
    public List<FSArray> arrays = new ArrayList<>();

    private FSObject() {}

    public FSObject(String name)
    {
        // TODO: Analyze and Comment
        this.size += 1 + 2 + 2 + 2;
        this.name(name);
    }

    public void addField(FSField field)
    {
        fields.add(field);
        size += field.size();
    }

    public void addString(FSString string)
    {
        strings.add(string);
        size += string.size();
    }

    public void addArray(FSArray array)
    {
        arrays.add(array);
        size += array.size();
    }

    public int getBytes(byte[] dest, int pointer)
    {
        pointer = writeBytes(dest, pointer, containerType);
        pointer = writeBytes(dest, pointer, nameLength);
        pointer = writeBytes(dest, pointer, name);
        pointer = writeBytes(dest, pointer, size);

        pointer = writeBytes(dest, pointer, fields.size());

        for (FSField field : fields)
            pointer = field.getBytes(dest, pointer);

        pointer = writeBytes(dest, pointer, strings.size());

        for (FSString string : strings)
            pointer = string.getBytes(dest, pointer);

        pointer = writeBytes(dest, pointer, arrays.size());

        for (FSArray array : arrays)
            pointer = array.getBytes(dest, pointer);

        return pointer;
    }

    public static FSObject Deserialize(byte[] data, int pointer)
    {
        pointer++; // Nudge the ContainerType out of the way

        FSObject result = new FSObject();
        result.nameLength = readShort(data, pointer);
        pointer += 2;
        result.name = readString(data, pointer, result.nameLength).getBytes();
        pointer += result.nameLength;

        result.size = readInt(data, pointer);
        pointer += 4;

        // Early-out: pointer += result.size - sizeOffset - result.nameLength;

        int fieldCount = readShort(data, pointer);

        pointer += 2;

        for (int i = 0; i < fieldCount; i++)
        {
            FSField field = FSField.Deserialize(data, pointer);
            result.fields.add(field);
            pointer += field.size();
        }

        int stringCount = readShort(data, pointer);

        pointer += 2;

        for (int i = 0; i < stringCount; i++)
        {
            FSString string = FSString.Deserialize(data, pointer);
            result.strings.add(string);
            pointer += string.size();
        }

        int arrayCount = readShort(data, pointer);

        pointer += 2;

        for (int i = 0; i < arrayCount; i++)
        {
            FSArray array = FSArray.Deserialize(data, pointer);
            result.arrays.add(array);
            pointer += array.size();
        }

        return result;
    }
}
