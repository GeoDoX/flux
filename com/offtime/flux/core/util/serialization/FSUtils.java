package com.offtime.flux.core.util.serialization;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public final class FSUtils
{
    public static final byte UNKNOWN = 0;

    public static final byte CONTAINER_DATABASE = 1;
    public static final byte CONTAINER_FIELD    = 1;
    public static final byte CONTAINER_OBJECT   = 1;
    public static final byte CONTAINER_ARRAY    = 1;
    public static final byte CONTAINER_STRING   = 1;

    public static final byte PRIMITIVE_BYTE 	= 1;
    public static final byte PRIMITIVE_SHORT 	= 2;
    public static final byte PRIMITIVE_CHAR 	= 3;
    public static final byte PRIMITIVE_INTEGER	= 4;
    public static final byte PRIMITIVE_LONG	 	= 5;
    public static final byte PRIMITIVE_FLOAT	= 6;
    public static final byte PRIMITIVE_DOUBLE	= 7;
    public static final byte PRIMITIVE_BOOLEAN	= 8;

    private FSUtils() {}

    public static byte getContainerType(Object o)
    {
        if (o.getClass().equals(FSDatabase.class))
            return CONTAINER_DATABASE;
        else if (o.getClass().equals(FSField.class))
            return CONTAINER_FIELD;
        else if (o.getClass().equals(FSObject.class))
            return CONTAINER_OBJECT;
        else if (o.getClass().equals(FSArray.class))
            return CONTAINER_ARRAY;
        else if (o.getClass().equals(FSString.class))
            return CONTAINER_STRING;

        else
            return UNKNOWN;
    }

    public static int getPrimitiveSize(byte type)
    {
        switch (type)
        {
            case PRIMITIVE_BYTE:
            case PRIMITIVE_BOOLEAN:
                return 1;
            case PRIMITIVE_SHORT:
            case PRIMITIVE_CHAR:
                return 2;
            case PRIMITIVE_INTEGER:
            case PRIMITIVE_FLOAT:
                return 4;
            case PRIMITIVE_DOUBLE:
            case PRIMITIVE_LONG:
                return 8;

            default:
                return 0;
        }
    }

    public static String getPrimitiveTypeAsString(byte type)
    {
        switch (type)
        {
            case UNKNOWN:
                return "Unknown";
            case PRIMITIVE_BYTE:
                return "Byte";
            case PRIMITIVE_SHORT:
                return "Short";
            case PRIMITIVE_CHAR:
                return "Char";
            case PRIMITIVE_INTEGER:
                return "Integer";
            case PRIMITIVE_LONG:
                return "Long";
            case PRIMITIVE_FLOAT:
                return "Float";
            case PRIMITIVE_DOUBLE:
                return "Double";
            case PRIMITIVE_BOOLEAN:
                return "Boolean";

            default:
                return "Invalid";
        }
    }

    public static int writeBytes(byte[] dest, int pointer, byte[] src)
    {
        for (int i = 0; i < src.length; i++)
            dest[pointer++] = src[i];

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, char[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, short[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, int[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, long[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, float[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, double[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, boolean[] src)
    {
        for (int i = 0; i < src.length; i++)
            pointer = writeBytes(dest, pointer, src[i]);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, byte value)
    {
        dest[pointer++] = value;

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, short value)
    {
        dest[pointer++] = (byte)((value >> 8) & 0xff);
        dest[pointer++] = (byte)((value >> 0) & 0xff);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, char value)
    {
        dest[pointer++] = (byte)((value >> 8) & 0xff);
        dest[pointer++] = (byte)((value >> 0) & 0xff);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, int value)
    {
        dest[pointer++] = (byte)((value >> 24) & 0xff);
        dest[pointer++] = (byte)((value >> 16) & 0xff);
        dest[pointer++] = (byte)((value >> 8) & 0xff);
        dest[pointer++] = (byte)((value >> 0) & 0xff);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, long value)
    {
        dest[pointer++] = (byte)((value >> 56) & 0xff);
        dest[pointer++] = (byte)((value >> 48) & 0xff);
        dest[pointer++] = (byte)((value >> 40) & 0xff);
        dest[pointer++] = (byte)((value >> 32) & 0xff);
        dest[pointer++] = (byte)((value >> 24) & 0xff);
        dest[pointer++] = (byte)((value >> 16) & 0xff);
        dest[pointer++] = (byte)((value >> 8) & 0xff);
        dest[pointer++] = (byte)((value >> 0) & 0xff);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, float value)
    {
        int data = Float.floatToIntBits(value);

        return writeBytes(dest, pointer, data);
    }

    public static int writeBytes(byte[] dest, int pointer, double value)
    {
        long data = Double.doubleToLongBits(value);

        return writeBytes(dest, pointer, data);
    }

    public static int writeBytes(byte[] dest, int pointer, boolean value)
    {
        dest[pointer++] = (byte) (value ? 1 : 0);

        return pointer;
    }

    public static int writeBytes(byte[] dest, int pointer, String string)
    {
        pointer = writeBytes(dest, pointer, (short) string.length());

        return writeBytes(dest, pointer, string.getBytes());
    }

    public static byte readByte(byte[] src, int pointer)
    {
        return src[pointer];
    }

    public static void readBytes(byte[] src, int pointer, byte[] dest)
    {
        for (int i = 0; i < dest.length; i++)
            dest[i] = src[pointer + i];
    }

    public static void readShorts(byte[] src, int pointer, short[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readShort(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_SHORT);
        }
    }

    public static void readChars(byte[] src, int pointer, char[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readChar(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_CHAR);
        }
    }

    public static void readInts(byte[] src, int pointer, int[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readInt(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_INTEGER);
        }
    }

    public static void readLongs(byte[] src, int pointer, long[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readLong(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_LONG);
        }
    }

    public static void readFloats(byte[] src, int pointer, float[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readFloat(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_FLOAT);
        }
    }

    public static void readDoubles(byte[] src, int pointer, double[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readDouble(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_DOUBLE);
        }
    }

    public static void readBooleans(byte[] src, int pointer, boolean[] dest)
    {
        for (int i = 0; i < dest.length; i++)
        {
            dest[i] = readBoolean(src, pointer);
            pointer += getPrimitiveSize(PRIMITIVE_BOOLEAN);
        }
    }
    public static short readShort(byte[] src, int pointer)
    {
        return ByteBuffer.wrap(src, pointer, 2).getShort();
    }

    public static char readChar(byte[] src, int pointer)
    {
        return ByteBuffer.wrap(src, pointer, 2).getChar();
    }

    public static int readInt(byte[] src, int pointer)
    {
        return ByteBuffer.wrap(src, pointer, 4).getInt();
    }

    public static long readLong(byte[] src, int pointer)
    {
        return ByteBuffer.wrap(src, pointer, 8).getLong();
    }

    public static float readFloat(byte[] src, int pointer)
    {
        return Float.intBitsToFloat(readInt(src, pointer));
    }

    public static double readDouble(byte[] src, int pointer)
    {
        return Double.longBitsToDouble(readLong(src, pointer));
    }

    public static boolean readBoolean(byte[] src, int pointer)
    {
        return src[pointer] != 0;
    }

    public static String readString(byte[] src, int pointer, int length)
    {
        return new String(src, pointer, length);
    }

    public static <T extends FSBase> T find(List<T> l, String name)
    {
        for (T obj : l)
        {
            if (obj.name().equals(name))
                return obj;
        }

        return null;
    }
}
