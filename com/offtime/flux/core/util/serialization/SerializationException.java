package com.offtime.flux.core.util.serialization;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public class SerializationException extends Exception
{
    public SerializationException(String e)
    {
        super(e);
    }
}
