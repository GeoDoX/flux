package com.offtime.flux.core.util.serialization;

import java.util.Objects;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public abstract class FSBase
{
    protected int nameLength;
    protected byte[] name;

    // size(nameLength[int]) + size(size[int])
    protected int size = 4 + 4;

    public void name(String name)
    {
        try
        {
            if (name.length() > Integer.MAX_VALUE)
                throw new SerializationException("Length must be less than " + Integer.MAX_VALUE + " bytes.");

            name = Objects.requireNonNull(name, "Name is null.");

            // If the name is already set, reset this.size
            if (this.name != null)
                this.size -= this.name.length;

            this.name = name.getBytes();
            this.nameLength = name.length();

            this.size += this.nameLength;
        }
        catch (SerializationException e)
        {
            e.printStackTrace();
        }
    }

    public String name()
    {
        return new String(this.name, 0, this.nameLength);
    }

    public int size()
    {
        return size;
    }
}
