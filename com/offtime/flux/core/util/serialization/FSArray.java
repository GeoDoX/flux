package com.offtime.flux.core.util.serialization;

import static com.offtime.flux.core.util.serialization.FSUtils.*;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public class FSArray extends FSBase
{
    public final byte containerType = getContainerType(this);

    public byte primitiveType;
    public int count;
    public byte[] data;

    private short[] shortData;
    private char[] charData;
    private int[] intData;
    private long[] longData;
    private float[] floatData;
    private double[] doubleData;
    private boolean[] booleanData;

    private FSArray()
    {
        size += 1 + 1 + 4;
    }

    private void updateSize()
    {
        size += getDataSize();
    }

    public int getBytes(byte[] dest, int pointer)
    {
        pointer = writeBytes(dest, pointer, containerType);
        pointer = writeBytes(dest, pointer, nameLength);
        pointer = writeBytes(dest, pointer, name);
        pointer = writeBytes(dest, pointer, size);
        pointer = writeBytes(dest, pointer, primitiveType);
        pointer = writeBytes(dest, pointer, count);

        switch(primitiveType)
        {
            case PRIMITIVE_BYTE:
                pointer = writeBytes(dest, pointer, data);
                break;
            case PRIMITIVE_SHORT:
                pointer = writeBytes(dest, pointer, shortData);
                break;
            case PRIMITIVE_CHAR:
                pointer = writeBytes(dest, pointer, charData);
                break;
            case PRIMITIVE_INTEGER:
                pointer = writeBytes(dest, pointer, intData);
                break;
            case PRIMITIVE_LONG:
                pointer = writeBytes(dest, pointer, longData);
                break;
            case PRIMITIVE_FLOAT:
                pointer = writeBytes(dest, pointer, floatData);
                break;
            case PRIMITIVE_DOUBLE:
                pointer = writeBytes(dest, pointer, doubleData);
                break;
            case PRIMITIVE_BOOLEAN:
                pointer = writeBytes(dest, pointer, booleanData);
                break;
        }

        return pointer;
    }

    public int getDataSize()
    {
        switch(primitiveType)
        {
            case PRIMITIVE_BYTE:
                return data.length * getPrimitiveSize(PRIMITIVE_BYTE);
            case PRIMITIVE_SHORT:
                return shortData.length * getPrimitiveSize(PRIMITIVE_SHORT);
            case PRIMITIVE_CHAR:
                return charData.length * getPrimitiveSize(PRIMITIVE_CHAR);
            case PRIMITIVE_INTEGER:
                return intData.length * getPrimitiveSize(PRIMITIVE_INTEGER);
            case PRIMITIVE_LONG:
                return longData.length * getPrimitiveSize(PRIMITIVE_LONG);
            case PRIMITIVE_FLOAT:
                return floatData.length * getPrimitiveSize(PRIMITIVE_FLOAT);
            case PRIMITIVE_DOUBLE:
                return doubleData.length * getPrimitiveSize(PRIMITIVE_DOUBLE);
            case PRIMITIVE_BOOLEAN:
                return booleanData.length * getPrimitiveSize(PRIMITIVE_BOOLEAN);
        }
        return 0;
    }

    public static FSArray Byte(String name, byte[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_BYTE;
        array.count = data.length;
        array.data = data;
        array.updateSize();

        return array;
    }

    public static FSArray Short(String name, short[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_SHORT;
        array.count = data.length;
        array.shortData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Char(String name, char[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_CHAR;
        array.count = data.length;
        array.charData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Integer(String name, int[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_INTEGER;
        array.count = data.length;
        array.intData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Long(String name, long[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_LONG;
        array.count = data.length;
        array.longData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Float(String name, float[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_FLOAT;
        array.count = data.length;
        array.floatData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Double(String name, double[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_DOUBLE;
        array.count = data.length;
        array.doubleData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Boolean(String name, boolean[] data)
    {
        FSArray array = new FSArray();

        array.name(name);
        array.primitiveType = PRIMITIVE_BOOLEAN;
        array.count = data.length;
        array.booleanData = data;
        array.updateSize();

        return array;
    }

    public static FSArray Deserialize(byte[] data, int pointer)
    {
        pointer++; // Nudge the ContainerType out of the way

        FSArray result = new FSArray();
        result.nameLength = readShort(data, pointer);
        pointer += 2;
        result.name = readString(data, pointer, result.nameLength).getBytes();
        pointer += result.nameLength;

        result.size = readInt(data, pointer);
        pointer += 4;

        result.primitiveType = data[pointer++];

        result.count = readInt(data, pointer);
        pointer += 4;

        switch(result.primitiveType)
        {
            case PRIMITIVE_BYTE:
                result.data = new byte[result.count];
                readBytes(data, pointer, result.data);
                break;
            case PRIMITIVE_SHORT:
                result.shortData = new short[result.count];
                readShorts(data, pointer, result.shortData);
                break;
            case PRIMITIVE_CHAR:
                result.charData = new char[result.count];
                readChars(data, pointer, result.charData);
                break;
            case PRIMITIVE_INTEGER:
                result.intData = new int[result.count];
                readInts(data, pointer, result.intData);
                break;
            case PRIMITIVE_LONG:
                result.longData = new long[result.count];
                readLongs(data, pointer, result.longData);
                break;
            case PRIMITIVE_FLOAT:
                result.floatData = new float[result.count];
                readFloats(data, pointer, result.floatData);
                break;
            case PRIMITIVE_DOUBLE:
                result.doubleData = new double[result.count];
                readDoubles(data, pointer, result.doubleData);
                break;
            case PRIMITIVE_BOOLEAN:
                result.booleanData = new boolean[result.count];
                readBooleans(data, pointer, result.booleanData);
                break;
        }

        pointer += result.count * getPrimitiveSize(result.primitiveType);

        return result;
    }
}
