package com.offtime.flux.core.util.serialization;

import static com.offtime.flux.core.util.serialization.FSUtils.*;

/**
 * Created by GeoDoX on 2019-01-11.
 */
public class FSString extends FSBase
{
    public final byte containerType = getContainerType(this);

    public int count;
    private char[] characters;

    private FSString()
    {
        size += 1 + 4;
    }

    public String asString()
    {
        return new String(characters);
    }

    private void updateSize()
    {
        size += getDataSize();
    }

    public int getBytes(byte[] dest, int pointer)
    {
        pointer = writeBytes(dest, pointer, containerType);
        pointer = writeBytes(dest, pointer, nameLength);
        pointer = writeBytes(dest, pointer, name);
        pointer = writeBytes(dest, pointer, size);
        pointer = writeBytes(dest, pointer, count);
        pointer = writeBytes(dest, pointer, characters);

        return pointer;
    }

    public int getDataSize()
    {
        return characters.length * getPrimitiveSize(PRIMITIVE_CHAR);
    }

    public static FSString Create(String name, String data)
    {
        FSString string = new FSString();

        string.name(name);
        string.count = data.length();
        string.characters = data.toCharArray();
        string.updateSize();

        return string;
    }

    public static FSString Deserialize(byte[] data, int pointer)
    {
        pointer++; // Nudge the ContainerType out of the way

        FSString result = new FSString();
        result.nameLength = readShort(data, pointer);
        pointer += 2;
        result.name = readString(data, pointer, result.nameLength).getBytes();
        pointer += result.nameLength;

        result.size = readInt(data, pointer);
        pointer += 4;

        result.count = readInt(data, pointer);
        pointer += 4;

        result.characters = new char[result.count];
        readChars(data, pointer, result.characters);

        pointer += result.count * getPrimitiveSize(PRIMITIVE_CHAR);

        return result;
    }

}
