package com.offtime.flux.core.util.random;

import java.util.Objects;
import java.util.Random;

/**
 * Created by GeoDoX on 2016-10-03.
 */
public class RandomUtils
{
    public static byte nextByte(Random random, byte min, byte max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextByte(random, min, max, true);
    }

    public static byte nextByte(Random random, byte min, byte max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return (byte) nextInt(random, min, max, inclusive);
    }

    public static char nextChar(Random random, char min, char max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextChar(random, min, max, true);
    }

    public static char nextChar(Random random, char min, char max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return (char) nextInt(random, min, max, inclusive);
    }

    public static short nextShort(Random random, short min, short max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextShort(random, min, max, true);
    }

    public static short nextShort(Random random, short min, short max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return (short) nextInt(random, min, max, inclusive);
    }

    public static int nextInt(Random random, int min, int max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextInt(random, min, max, true);
    }

    public static int nextInt(Random random, int min, int max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        if (inclusive)
            return random.nextInt(max + 1 - min) + min;
        else
            return random.nextInt(max - min) + min;
    }

    public static float nextFloat(Random random, float min, float max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextFloat(random, min, max, true);
    }

    public static float nextFloat(Random random, float min, float max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        if (inclusive)
            return random.nextFloat() * (max + 1 - min) + min;
        else
            return random.nextFloat() * (max - min) + min;
    }

    public static double nextDouble(Random random, double min, double max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextDouble(random, min, max, true);
    }

    public static double nextDouble(Random random, double min, double max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        if (inclusive)
            return random.nextDouble() * (max + 1 - min) + min;
        else
            return random.nextDouble() * (max - min) + min;
    }

    public static long nextLong(Random random, long min, long max)
    {
        random = Objects.requireNonNull(random, "Null Random");

        return nextLong(random, min, max, true);
    }

    public static long nextLong(Random random, long min, long max, boolean inclusive)
    {
        random = Objects.requireNonNull(random, "Null Random");

        if (inclusive)
            return random.nextLong() * (max + 1 - min) + min;
        else
            return random.nextLong() * (max - min) + min;
    }
}
