package com.offtime.flux.core.util;

/**
 * Created by GeoDoX on 2018-01-20.
 */
public class Triplet<L, M, R>
{
    public L first;
    public M second;
    public R third;

    public Triplet(L first, M second, R third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public void set(L first, M second, R third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @SuppressWarnings("unchecked")
    public static <L, M, R> Triplet<L, M, R>[] build(Object[][] tuples) throws ClassCastException
    {
        Triplet<L, M, R>[] result = (Triplet<L, M, R>[]) new Triplet[tuples.length];

        for (int i = 0; i < result.length; i++)
            result[i] = new Triplet<>((L) tuples[i][0], (M) tuples[i][1], (R) tuples[i][2]);

        return result;
    }

    public static <L, M, R> void forEach(Triplet<L, M, R>[] triplets, TripletCallback<L, M, R> callback)
    {
        for (Triplet<L, M, R> triplet : triplets)
            callback.invoke(triplet.first, triplet.second, triplet.third);
    }

    @SuppressWarnings("unchecked")
    public static <L, M, R> Triplet<L, M, R>[] tripletwiseConcat(L[] array0, M[] array1, R[] array2)
    {
        Triplet<L, M, R>[] result = (Triplet<L, M, R>[]) new Triplet[Math.min(array0.length, array1.length)];

        for (int i = 0; i < result.length; i++)
            result[i] = new Triplet<>(array0[i], array1[i], array2[i]);

        return result;
    }

    public interface TripletCallback<L, M, R>
    {
        void invoke(L first, M second, R third);
    }
}
