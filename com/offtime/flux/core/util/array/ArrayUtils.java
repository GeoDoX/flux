package com.offtime.flux.core.util.array;

import com.offtime.flux.core.util.Pair;

/**
 * Created by GeoDoX on 2017-07-31.
 */
public class ArrayUtils
{
    /**
     * Converts a 2D index to a 1D index.
     *
     * @param indexX x index
     * @param indexY y index
     * @param arrayWidth Width of array
     * @return 1D index
     */
    public static int index2DTo1D(int indexX, int indexY, int arrayWidth)
    {
        return arrayWidth * indexY + indexX;
    }

    /**
     * Converts a 1D index to a 2D index.
     *
     * @param index index
     * @param arrayWidth width of array
     * @return 2D index
     */
    public static int index1DTo2DX(int index, int arrayWidth)
    {
        return index % arrayWidth;
    }

    /**
     * Converts a 1D index to a 2D index.
     *
     * @param index index
     * @param arrayWidth width of array
     * @return 2D index
     */
    public static int index1DTo2DY(int index, int arrayWidth)
    {
        return index / arrayWidth;
    }

    public static Object[][] zip(Object[]... arrays)
    {
        int minLength = Integer.MAX_VALUE;

        for(Object[] array : arrays)
        {
            if(minLength > array.length)
                minLength = array.length;
        }

        Object[][] result = new Object[minLength][arrays.length];

        if(minLength == 0)
            return result;

        for(int i = 0; i < minLength; i++)
        {
            for(int j = 0; j < arrays.length; j++)
            {
                result[i][j] = arrays[j][i];
            }
        }

        return result;
    }

    @SafeVarargs
    public static <T> T coalesce(T ...items)
    {
        for(T i : items)
            if(i != null) return i;

        return null;
    }
}
