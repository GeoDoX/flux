package com.offtime.flux.core.input;

/**
 * Created by GeoDoX on 2016-09-27.
 */
public class MouseState
{
    public static final MouseState PRESSED = new MouseState("Pressed");
    public static final MouseState RELEASED = new MouseState("Released");
    public static final MouseState DRAGGED = new MouseState("Dragged");
    public static final MouseState MOVED = new MouseState("Moved");

    private String state;

    private MouseState(String state)
    {
        this.state = state;
    }

    public String stateName()
    {
        return state;
    }

    @Override
    public String toString()
    {
        return state;
    }
}
