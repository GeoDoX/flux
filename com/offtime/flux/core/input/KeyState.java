package com.offtime.flux.core.input;

/**
 * Created by GeoDoX on 2016-10-23.
 */
public enum KeyState
{
    PRESSED("Pressed"),
    RELEASED("Released");

    public final String state;

    KeyState(String state)
    {
        this.state = state;
    }

    @Override
    public String toString()
    {
        return state;
    }
}
