package com.offtime.flux.core.input;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by GeoDoX on 2019-01-03.
 */
public class Keys
{
    protected static final Map<Object, Key> keyMap = new HashMap<>(0);

    public static Key map(Key k, Object key)
    {
        k.keyCode = key;
        keyMap.put(key, k);

        return k;
    }

    public static Key UNDEFINED     = map(new Key("Undefined"), null);

    public static Key ENTER             = new Key("Enter");
    public static Key BACKSPACE         = new Key("Backspace");
    public static Key TAB               = new Key("Tab");
    public static Key PAUSE             = new Key("Pause");
    public static Key CAPS_LOCK         = new Key("Caps Lock");
    public static Key NUM_LOCK          = new Key("Num Lock");
    public static Key SCROLL_LOCK       = new Key("Scroll Lock");
    public static Key ESCAPE            = new Key("Escape");
    public static Key SPACE             = new Key("Space");
    public static Key PAGE_UP           = new Key("Page Up");
    public static Key PAGE_DOWN         = new Key("Page Down");
    public static Key END               = new Key("End");
    public static Key HOME              = new Key("Home");
    public static Key COMMA             = new Key(",");
    public static Key PERIOD            = new Key(".");
    public static Key MINUS             = new Key("-");
    public static Key EQUALS            = new Key("=");
    public static Key SLASH             = new Key("/");
    public static Key BACKSLASH         = new Key("\\");
    public static Key SEMICOLON         = new Key(";");
    public static Key OPEN_BRACKET      = new Key("[");
    public static Key CLOSE_BRACKET     = new Key("]");
    public static Key DELETE            = new Key("Delete");
    public static Key QUOTE             = new Key("'");
    public static Key BACK_QUOTE        = new Key("`");
    public static Key PRINT_SCREEN      = new Key("Print Screen");
    public static Key INSERT            = new Key("Insert");
    public static Key OS                = new Key("OS");

    public static Key LEFT              = new Key("Left");
    public static Key RIGHT             = new Key("Right");
    public static Key UP                = new Key("Up");
    public static Key DOWN              = new Key("Down");
    public static Key NUM_PAD_LEFT      = new Key("Numpad Left");
    public static Key NUM_PAD_RIGHT     = new Key("Numpad Right");
    public static Key NUM_PAD_UP        = new Key("Numpad Up");
    public static Key NUM_PAD_DOWN      = new Key("Numpad Down");

    public static Key ZERO              = new Key("0");
    public static Key ONE               = new Key("1");
    public static Key TWO               = new Key("2");
    public static Key THREE             = new Key("3");
    public static Key FOUR              = new Key("4");
    public static Key FIVE              = new Key("5");
    public static Key SIX               = new Key("6");
    public static Key SEVEN             = new Key("7");
    public static Key EIGHT             = new Key("8");
    public static Key NINE              = new Key("9");

    public static Key NUM_PAD_ZERO      = new Key("0");
    public static Key NUM_PAD_ONE       = new Key("1");
    public static Key NUM_PAD_TWO       = new Key("2");
    public static Key NUM_PAD_THREE     = new Key("3");
    public static Key NUM_PAD_FOUR      = new Key("4");
    public static Key NUM_PAD_FIVE      = new Key("5");
    public static Key NUM_PAD_SIX       = new Key("6");
    public static Key NUM_PAD_SEVEN     = new Key("7");
    public static Key NUM_PAD_EIGHT     = new Key("8");
    public static Key NUM_PAD_NINE      = new Key("9");

    public static Key NUM_PAD_MULTIPLY  = new Key("*");
    public static Key NUM_PAD_DIVIDE    = new Key("/");
    public static Key NUM_PAD_ADD       = new Key("+");
    public static Key NUM_PAD_SUBTRACT  = new Key("-");

    public static Key A                 = new Key("A");
    public static Key B                 = new Key("B");
    public static Key C                 = new Key("C");
    public static Key D                 = new Key("D");
    public static Key E                 = new Key("E");
    public static Key F                 = new Key("F");
    public static Key G                 = new Key("G");
    public static Key H                 = new Key("H");
    public static Key I                 = new Key("I");
    public static Key J                 = new Key("J");
    public static Key K                 = new Key("K");
    public static Key L                 = new Key("L");
    public static Key M                 = new Key("M");
    public static Key N                 = new Key("N");
    public static Key O                 = new Key("O");
    public static Key P                 = new Key("P");
    public static Key Q                 = new Key("Q");
    public static Key R                 = new Key("R");
    public static Key S                 = new Key("S");
    public static Key T                 = new Key("T");
    public static Key U                 = new Key("U");
    public static Key V                 = new Key("V");
    public static Key W                 = new Key("W");
    public static Key X                 = new Key("X");
    public static Key Y                 = new Key("Y");
    public static Key Z                 = new Key("Z");

    public static Key F1                = new Key("F1");
    public static Key F2                = new Key("F2");
    public static Key F3                = new Key("F3");
    public static Key F4                = new Key("F4");
    public static Key F5                = new Key("F5");
    public static Key F6                = new Key("F6");
    public static Key F7                = new Key("F7");
    public static Key F8                = new Key("F8");
    public static Key F9                = new Key("F9");
    public static Key F10               = new Key("F10");
    public static Key F11               = new Key("F11");
    public static Key F12               = new Key("F12");
    public static Key F13               = new Key("F13");
    public static Key F14               = new Key("F14");
    public static Key F15               = new Key("F15");
    public static Key F16               = new Key("F16");
    public static Key F17               = new Key("F17");
    public static Key F18               = new Key("F18");
    public static Key F19               = new Key("F19");
    public static Key F20               = new Key("F20");
    public static Key F21               = new Key("F21");
    public static Key F22               = new Key("F22");
    public static Key F23               = new Key("F23");
    public static Key F24               = new Key("F24");

    public static class Key
    {
        public final String name;
        private Object keyCode;

        public boolean control;
        public boolean shift;
        public boolean alt;

        private Key(String name)
        {
            this.name = name;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            Key key = (Key) o;

            return (Objects.equals(name, key.name)       &&
                    Objects.equals(keyCode, key.keyCode) &&
                    Objects.equals(control, key.control) &&
                    Objects.equals(shift, key.shift)     &&
                    Objects.equals(alt, key.alt));
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(name, keyCode);
        }

        @Override
        public String toString()
        {
            return name;
        }
    }

    public static Keys.Key fromKeyCode(Object keyCode)
    {
        Keys.Key fromKeyCode = Keys.keyMap.get(keyCode);

        if (fromKeyCode == null)
            return Keys.UNDEFINED;

        return fromKeyCode;
    }
}
