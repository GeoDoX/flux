package com.offtime.flux.core.input;

import com.offtime.flux.core.event.EventHandler;
import com.offtime.flux.core.event.mouse.MouseEvent;

/**
 * Created by GeoDoX on 2019-01-03.
 */
public class Mouse
{
    public static int x, y;
    public static boolean dragging;
    public static boolean primary, middle, secondary;

    public static boolean pressed()
    {
        return (primary || middle || secondary) && !dragging;
    }

    @EventHandler
    public void onMouseEvent(MouseEvent event)
    {
        handle(event);
    }

    private void handle(MouseEvent event)
    {
        x = event.x;
        y = event.y;

        dragging = event.state == MouseState.DRAGGED;

        if (event.state == MouseState.PRESSED || event.state == MouseState.DRAGGED)
        {
            if (event.button == MouseButton.PRIMARY)
                primary = true;
            else if (event.button == MouseButton.MIDDLE)
                middle = true;
            else if (event.button == MouseButton.SECONDARY)
                secondary = true;
        }
        else if (event.state == MouseState.RELEASED)
        {
            if (event.button == MouseButton.PRIMARY)
                primary = false;
            else if (event.button == MouseButton.MIDDLE)
                middle = false;
            else if (event.button == MouseButton.SECONDARY)
                secondary = false;
        }
    }
}
