package com.offtime.flux.core.input;

/**
 * Created by GeoDoX on 2016-09-22.
 */
public class MouseButton
{
    public static final MouseButton UNDEFINED = new MouseButton("Undefined");
    public static final MouseButton PRIMARY = new MouseButton("Primary");
    public static final MouseButton MIDDLE = new MouseButton("Middle");
    public static final MouseButton SECONDARY = new MouseButton("Secondary");

    private String name;

    private MouseButton(String name)
    {
        this.name = name;
    }

    public String name()
    {
        return name;
    }
}
