package com.offtime.flux.core;

/**
 * Created by GeoDoX on 2017-12-15.
 */
interface IProperties
{
    IProperties copy();
}
