package com.offtime.flux.core.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public class Directory extends File
{
    public Directory(String path)
    {
        super(path);
    }

    @Override
    public void create() throws IOException
    {
        if (!exists())
            Files.createDirectory(Paths.get(this.path));
    }

    public List<File> files()
    {
        java.io.File file = asFile();
        List<File> files = new ArrayList<>(0);

        if (file == null)
            return files;

        java.io.File[] ioFiles = file.listFiles();

        if (ioFiles == null)
            return files;

        for (java.io.File ioFile : ioFiles)
            files.add(new File(ioFile.getAbsolutePath()));

        return files;
    }

    @Override
    public InputStream inputStream() throws IOException
    {
        throw new UnsupportedOperationException("Cannot create an input stream on a Directory");
    }

    @Override
    public OutputStream outputStream()
    {
        throw new UnsupportedOperationException("Cannot create an output stream on a Directory");
    }
}
