package com.offtime.flux.core.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public class File
{
    protected String path;

    public File(String path)
    {
        this.path = Objects.requireNonNull(path, "Null Path");
    }

    public void create() throws IOException
    {
        if (!exists())
            Files.createFile(asPath());
    }

    public void delete() throws IOException
    {
        if (exists())
            Files.delete(asPath());
    }

    public boolean exists()
    {
        return Files.exists(Paths.get(this.path));
    }

    public String path()
    {
        return path;
    }

    public long lastModified()
    {
        if (exists())
            return asFile().lastModified();

        return 0;
    }

    public Directory directory()
    {
        return new Directory(asFile().getParentFile().toPath().toString());
    }

    public InputStream inputStream() throws IOException
    {
        return java.nio.file.Files.newInputStream(asPath());
    }

    public OutputStream outputStream() throws IOException
    {
        return java.nio.file.Files.newOutputStream(asPath());
    }

    public java.io.File asFile()
    {
        return new java.io.File(this.path);
    }

    public java.nio.file.Path asPath()
    {
        return Paths.get(this.path);
    }

    public boolean isFile()
    {
        return asFile().isFile();
    }

    public boolean isDirectory()
    {
        return asFile().isDirectory();
    }
}
