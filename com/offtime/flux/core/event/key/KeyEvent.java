package com.offtime.flux.core.event.key;

import com.offtime.flux.core.event.Event;
import com.offtime.flux.core.input.KeyState;
import com.offtime.flux.core.input.Keys;

import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class KeyEvent implements Event
{
    public final Keys.Key key;
    public final KeyState state;
    public final boolean control;
    public final boolean shift;
    public final boolean alt;

    public KeyEvent(KeyState state, Keys.Key key)
    {
        this.state = state;
        this.key = key;
        this.control = key.control;
        this.shift = key.shift;
        this.alt = key.alt;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        KeyEvent other = (KeyEvent) o;

        return (Objects.equals(key, other.key)         &&
                Objects.equals(state, other.state)     &&
                Objects.equals(control, other.control) &&
                Objects.equals(shift, other.shift)     &&
                Objects.equals(alt, other.alt));
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(state, key, control, shift, alt);
    }

    @Override
    public String toString()
    {
        String toString = "[KeyEvent] Key: " + key + ", State: " + state;

        if (control || shift || alt)
            toString += ", Modifiers: ";

        if (control)
            toString += "Control ";
        if (shift)
            toString += "Shift ";
        if (alt)
            toString += "Alt";

        return toString;
    }
}