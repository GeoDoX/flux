package com.offtime.flux.core.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by GeoDoX on 2016-09-20.
 */
class EventHandle
{
    public final Method eventMethod;
    public final Object methodClass;

    public EventHandle(Method eventMethod, Object methodClass) {
        this.eventMethod = eventMethod;
        this.methodClass = methodClass;
    }

    public void invokeEvent(Event eventInstance) throws InvocationTargetException, IllegalAccessException
    {
        eventMethod.invoke(methodClass, eventInstance);
    }
}
