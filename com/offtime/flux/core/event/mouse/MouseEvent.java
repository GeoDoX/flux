package com.offtime.flux.core.event.mouse;

import com.offtime.flux.core.event.Event;
import com.offtime.flux.core.input.MouseButton;
import com.offtime.flux.core.input.MouseState;

import java.util.Objects;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class MouseEvent implements Event
{
    public final MouseState state;
    public final MouseButton button;
    public final int x;
    public final int y;

    public final int px;
    public final int py;
    public final int rx;
    public final int ry;

    public MouseEvent(MouseState state, MouseButton button, int x, int y, int px, int py, int rx, int ry)
    {
        this.state = state;
        this.button = button;
        this.x = x;
        this.y = y;
        this.px = px;
        this.py = py;
        this.rx = rx;
        this.ry = ry;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        MouseEvent other = (MouseEvent) o;

        return (Objects.equals(x, other.x)          &&
                Objects.equals(y, other.y)          &&
                Objects.equals(state, other.state)  &&
                Objects.equals(button, other.button));
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(state, button, x, y);
    }

    @Override
    public String toString()
    {
        return "[MouseEvent] Button: " + button + ", State: " + state + " (" + x + ", " + y + ")";
    }
}
