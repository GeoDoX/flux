package com.offtime.flux.core.event.window;

import com.offtime.flux.core.event.Event;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class WindowVisibilityEvent implements Event
{
    public final State state;

    public WindowVisibilityEvent(State state)
    {
        this.state = state;
    }

    public enum State
    {
        Visible,
        Hidden;

        public static State get(boolean visible)
        {
            if (visible)
                return Visible;
            else
                return Hidden;
        }
    }
}
