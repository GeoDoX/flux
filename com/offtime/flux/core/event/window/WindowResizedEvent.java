package com.offtime.flux.core.event.window;

import com.offtime.flux.core.event.Event;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class WindowResizedEvent implements Event
{
    public final int width;
    public final int height;

    public WindowResizedEvent(int width, int height)
    {
        this.width = width;
        this.height = height;
    }
}
