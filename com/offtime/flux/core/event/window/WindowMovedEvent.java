package com.offtime.flux.core.event.window;

import com.offtime.flux.core.event.Event;

/**
 * Created by GeoDoX on 2017-12-17.
 */
public class WindowMovedEvent implements Event
{
    public final int x;
    public final int y;

    public WindowMovedEvent(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
