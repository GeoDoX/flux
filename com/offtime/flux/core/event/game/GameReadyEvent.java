package com.offtime.flux.core.event.game;

import com.offtime.flux.core.FluxCore;
import com.offtime.flux.core.event.Event;

/**
 * Created by GeoDoX on 2019-01-30.
 */
public class GameReadyEvent implements Event
{
    public final FluxCore game;

    public GameReadyEvent(FluxCore game)
    {
        this.game = game;
    }
}
