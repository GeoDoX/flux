package com.offtime.flux.core.event;

import com.offtime.flux.core.FluxCore;
import com.offtime.flux.core.util.logging.ILogger;
import com.offtime.flux.core.util.logging.LoggerTag;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by GeoDoX on 2016-02-15.
 */
public class EventSystem
{
    private static final LoggerTag LOGGER_TAG = new LoggerTag("Flux.Core.EventSystem");

    private static final Map<Class<?>, List<EventHandle>> eventHandlerMap = new HashMap<>(0);
    private static final Map<Class<?>, List<EventHandle>> eventHandlerMapTemp = new HashMap<>(0);
    private static final List<Object> markedForUnregister = new ArrayList<>(0);

    private static final Map<String, List<Event>> delayedEvents = new HashMap<>(0);
    private static final Map<String, List<Event>> delayedEventsTemp = new HashMap<>(0);

    private static ILogger _logger;

    public static void register(Object object, Class... eventClasses)
    {
        for (Class eventClass : eventClasses)
            register(object, eventClass);
    }

    public static void register(Object object, Class eventClass)
    {
        object = Objects.requireNonNull(object, "Null Object");
        eventClass = Objects.requireNonNull(eventClass, "Null Event Class");

        for(Method method : object.getClass().getDeclaredMethods())
        {
            if(method.isAnnotationPresent(EventHandler.class) && method.getParameterTypes().length > 0 && method.getParameterTypes()[0].equals(eventClass))
            {
                method.setAccessible(true);

                if (!eventHandlerMap.containsKey(eventClass))
                    eventHandlerMap.put(eventClass, new ArrayList<>(0));

                eventHandlerMap.get(eventClass).add(new EventHandle(method, object));
            }
        }
    }

    public static void unregister(Object object)
    {
        markedForUnregister.add(object);
    }

    private static void _unregister(Object object)
    {
        object = Objects.requireNonNull(object, "Null Object");

        Iterator<EventHandle> listenerEntryEventHandleIterator;
        EventHandle listenerEntryEventHandle;
        for (Map.Entry<Class<?>, List<EventHandle>> listenerEntry : eventHandlerMap.entrySet())
        {
            listenerEntryEventHandleIterator = listenerEntry.getValue().iterator();

            while (listenerEntryEventHandleIterator.hasNext())
            {
                listenerEntryEventHandle = listenerEntryEventHandleIterator.next();

                if (listenerEntryEventHandle.methodClass.equals(object))
                    listenerEntryEventHandleIterator.remove();
            }
        }
    }

    public static void dispatch(Event event)
    {
        event = Objects.requireNonNull(event, "Null Event");

        eventHandlerMapTemp.clear();
        eventHandlerMapTemp.putAll(eventHandlerMap);

        for (Class<?> clazz : eventHandlerMapTemp.keySet())
        {
            if (clazz.equals(event.getClass()))
            {
                for (EventHandle eventHandle : eventHandlerMapTemp.get(clazz))
                {
                    try
                    {
                        eventHandle.invokeEvent(event);
                    }
                    catch (InvocationTargetException | IllegalAccessException e)
                    {
                        if (_logger == null)
                            e.printStackTrace();
                        else
                        {
                            _logger.log(LOGGER_TAG, "Exception thrown while attempting to dispatch Event");
                            _logger.log(LOGGER_TAG, e);
                        }
                    }
                }
            }
        }

        if (markedForUnregister.size() != 0)
        {
            for (Object object : markedForUnregister)
                _unregister(object);
        }
    }

    public static void delayEvent(String key, Event event)
    {
        event = Objects.requireNonNull(event, "Null Event");

        if (!delayedEvents.containsKey(key))
            delayedEvents.put(key, new ArrayList<>(0));

        delayedEvents.get(key).add(event);
    }

    public static void dispatchDelayedEvents(String key)
    {
        if (!delayedEvents.containsKey(key))
            return;

        delayedEventsTemp.clear();
        delayedEventsTemp.putAll(delayedEvents);

        try
        {
            for (Event event : delayedEventsTemp.get(key))
                dispatch(event);
        }
        catch (ConcurrentModificationException | NullPointerException ignored) {}

        delayedEvents.clear();
    }

    public static void dispose()
    {
        if (eventHandlerMap != null)
            eventHandlerMap.clear();

        if (eventHandlerMapTemp != null)
            eventHandlerMapTemp.clear();

        if (markedForUnregister != null)
            markedForUnregister.clear();

        if (delayedEvents != null)
            delayedEvents.clear();
    }

    public static void logger(ILogger logger)
    {
        _logger = logger;
    }
}
