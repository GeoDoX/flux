package com.offtime.flux.core;

/**
 * Created by GeoDoX on 2017-12-12.
 */
public interface ILauncher
{
    void launch(FluxCore game);
}
