package com.offtime.flux.core.window;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public interface IWindow
{
    void update();
    void draw();
    void dispose();

    boolean closeRequested();
    void requestClose();

    String title();
    IWindow title(String title);

    IRenderer renderer();
    IWindow renderer(IRenderer renderer);

    boolean visible();
    IWindow visible(boolean visible);

    boolean resizable();
    IWindow resizeable(boolean resizable);

    int x();
    IWindow x(int x);

    int y();
    IWindow y(int y);

    int width();
    IWindow width(int width);

    int height();
    IWindow height(int height);
}
