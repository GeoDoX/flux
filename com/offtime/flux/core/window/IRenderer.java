package com.offtime.flux.core.window;

import java.awt.Color;

/**
 * Created by GeoDoX on 2017-12-11.
 */
public interface IRenderer
{
    IWindow window();

    void clear(int x, int y, int width, int height);
    void clear(int x, int y, int width, int height, Color color);

    void push();
    void translate(int x, int y);
    void scale(float x, float y);
    void pop();
}
