package com.offtime.flux.profiling;

/**
 * Created by GeoDoX on 2017-12-15.
 */
public class ProfilerTask
{
    public final String taskName;

    private long startTime = 0;
    private long finishTime = 0;

    public ProfilerTask(String taskName)
    {
        this.taskName = taskName;
    }

    public void start()
    {
        startTime = System.nanoTime();
    }

    public void stop()
    {
        finishTime = System.nanoTime();
    }

    public long startTime()
    {
        return startTime;
    }

    public long finishedTime()
    {
        return finishTime;
    }

    public long elapsedTime()
    {
        return finishTime - startTime;
    }

    @Override
    public String toString()
    {
        if (finishTime < startTime)
            return "[" + taskName + "] Status: Incomplete";

        return "[" + taskName + "] Elapsed: " + elapsedTime();
    }
}
