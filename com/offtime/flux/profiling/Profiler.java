package com.offtime.flux.profiling;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by GeoDoX on 2017-12-15.
 */
public class Profiler
{
    // Stores a percentage value of how long the associated task took that update
    private static Map<ProfilerTask, Float> tasks = new HashMap<>(0);

    private static long preUpdateTime, postUpdateTime;

    public static void pre()
    {
        preUpdateTime = System.nanoTime();
    }

    public static void post()
    {
        postUpdateTime = System.nanoTime();
    }

    public static void calculate()
    {
        long totalUpdateTime = postUpdateTime - preUpdateTime;

        for (ProfilerTask task : tasks.keySet())
            tasks.put(task, (float) (task.elapsedTime() / totalUpdateTime));
    }

    public static void track(ProfilerTask task)
    {
        tasks.put(task, 0f);
    }

    public static void untrack(ProfilerTask task)
    {
        tasks.remove(task);
    }

    public static Map<ProfilerTask, Float> tasks()
    {
        return new HashMap<>(tasks);
    }

}
