package com.offtime.flux.opengl.window;

import com.offtime.flux.core.window.IRenderer;
import com.offtime.flux.core.window.IWindow;
import com.offtime.flux.opengl.FluxGame;
import org.lwjgl.BufferUtils;

import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by GeoDoX on 2017-12-27.
 */
public class Window implements IWindow
{
    protected final FluxGame game;

    protected long handle;

    protected String title;
    protected boolean visible;
    protected int minimumWidth, minimumHeight;
    protected int maximumWidth, maximumHeight;

    protected long monitor;

    protected IntBuffer x;
    protected IntBuffer y;
    protected IntBuffer width;
    protected IntBuffer height;

    public Window(FluxGame game, String title, int width, int height, boolean visible)
    {
        this(game, title, width, height, visible, true, glfwGetPrimaryMonitor());
    }

    public Window(FluxGame game, String title, int width, int height, boolean visible, boolean resizable)
    {
        this(game, title, width, height, visible, resizable, glfwGetPrimaryMonitor());
    }

    public Window(FluxGame game, String title, int width, int height, boolean visible, boolean resizable, long monitor)
    {
        System.out.println("Window Create");
        this.handle = glfwCreateWindow(width, height, title, NULL, NULL);

        this.game = game;

        this.monitor = monitor;

        this.x = BufferUtils.createIntBuffer(1);
        this.y = BufferUtils.createIntBuffer(1);
        this.width = BufferUtils.createIntBuffer(1);
        this.height = BufferUtils.createIntBuffer(1);

        this.title(title);
        this.visible(visible);
        this.resizeable(resizable);

        glfwSetErrorCallback(this::errorCallback);
    }

    public void errorCallback(int error, long description)
    {
        System.out.println("Error: " + error + ", Description: " + description);
    }

    public void swap()
    {
        glfwSwapBuffers(this.handle);
    }

    public void current()
    {
        glfwMakeContextCurrent(this.handle);
    }

    public long monitor()
    {
        this.monitor = glfwGetWindowMonitor(this.handle);

        return this.monitor;
    }

    public int monitorWidth()
    {
        return glfwGetVideoMode(monitor()).width();
    }

    public int monitorHeight()
    {
        return glfwGetVideoMode(monitor()).height();
    }

    public void beginDraw()
    {
        glfwMakeContextCurrent(this.handle);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void update()
    {

    }

    @Override
    public void draw()
    {
        glfwSwapBuffers(this.handle);
    }

    @Override
    public void dispose()
    {
        //glfwDestroyWindow(this.handle);
    }

    @Override
    public String title()
    {
        return title;
    }

    @Override
    public IWindow title(String title)
    {
        this.title = title;

        glfwSetWindowTitle(this.handle, this.title);

        return this;
    }

    @Override
    public IRenderer renderer()
    {
        return null;
    }

    @Override
    public IWindow renderer(IRenderer renderer)
    {
        return null;
    }

    @Override
    public boolean visible()
    {
        return visible;
    }

    @Override
    public IWindow visible(boolean visible)
    {
        this.visible = visible;

        if (this.visible)
            glfwShowWindow(this.handle);
        else
            glfwHideWindow(this.handle);

        return this;
    }

    @Override
    public boolean resizable()
    {
        return (minimumWidth == maximumWidth) && (minimumHeight == maximumHeight);
    }

    @Override
    public IWindow resizeable(boolean resizable)
    {
        this.minimumWidth = resizable ? 0 : width();
        this.minimumHeight = resizable ? 0 : height();
        this.maximumWidth = resizable ? 0 : monitorWidth();
        this.maximumHeight = resizable ? 0 : monitorHeight();

        return this;
    }

    @Override
    public boolean closeRequested()
    {
        // BUG: Causes Infinite Loop in FluxCore.run()#while(running())
        return glfwWindowShouldClose(this.handle);
    }

    @Override
    public void requestClose()
    {
        glfwSetWindowShouldClose(this.handle, true);
    }

    @Override
    public int x()
    {
        glfwGetWindowPos(this.handle, x, y);

        return x.get(0);
    }

    @Override
    public IWindow x(int x)
    {
        glfwSetWindowPos(this.handle, x, y());

        return this;
    }

    @Override
    public int y()
    {
        glfwGetWindowPos(this.handle, x, y);

        return y.get(0);
    }

    @Override
    public IWindow y(int y)
    {
        glfwSetWindowPos(this.handle, x(), y);

        return this;
    }

    @Override
    public int width()
    {
        glfwGetWindowPos(this.handle, width, height);

        return width.get(0);
    }

    @Override
    public IWindow width(int width)
    {
        glfwSetWindowPos(this.handle, width, height());

        return this;
    }

    @Override
    public int height()
    {
        glfwGetWindowPos(this.handle, width, height);

        return height.get(0);
    }

    @Override
    public IWindow height(int height)
    {
        glfwSetWindowPos(this.handle, width(), height);

        return this;
    }
}
