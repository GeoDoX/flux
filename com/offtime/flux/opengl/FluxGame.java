package com.offtime.flux.opengl;

import com.offtime.flux.core.FluxCore;
import com.offtime.flux.core.event.EventSystem;
import com.offtime.flux.core.event.game.GameReadyEvent;
import com.offtime.flux.core.mod.ModRef;
import com.offtime.flux.core.mod.ModRegistry;
import com.offtime.flux.core.util.logging.ILogger;
import com.offtime.flux.core.util.time.TimeUtils;
import com.offtime.flux.opengl.util.GLFWUtils;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by GeoDoX on 2017-12-27.
 */
public abstract class FluxGame extends FluxCore
{
    public FluxGame()
    {
        super(new Properties());
    }

    public FluxGame(Properties properties)
    {
        super(properties);
    }

    public FluxGame(Properties properties, ILogger logger)
    {
        super(properties, logger);
    }

    public void _preInitialize()
    {
        super._preInitialize();

        if (!glfwInit())
            throw new IllegalStateException("Error Initializing GLFW");

        System.out.println("Window Hint");
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    }

    @Override
    public void start()
    {
        _preInitialize();
        _initialize();
        _postInitialize();

        this.deltaTime = 0;

        int targetFPS = properties.targetFPS;
        int targetUPS = properties.targetUPS;

        if (!properties().limitFPS)
            targetFPS = properties.maxFPS;

        long now;
        long lastTime = System.nanoTime();
        long dUAccumulator = 0;
        long dFAccumulator = 0;
        long delta;
        long nsU = 1000000000 / targetUPS; // nano seconds per update
        long nsF = 1000000000 / targetFPS; // nano seconds per render
        long timer = System.currentTimeMillis();
        int updates = 0;
        int frames = 0;

        logger.log(LOGGER_TAG, "Game Ready");

        EventSystem.dispatch(new GameReadyEvent(this));

        logger.log(LOGGER_TAG, "Starting Game Loop");

        while (running())
        {
            now = System.nanoTime();
            delta = now - lastTime;
            dUAccumulator += delta;
            dFAccumulator += delta;
            lastTime = now;

            if (dUAccumulator >= nsU)
            {
                this.deltaTime = TimeUtils.calculateDeltaTime(dUAccumulator);

                update();
                dUAccumulator -= nsU;
                updates++;
            }

            if (dFAccumulator >= nsF)
            {
                dFAccumulator -= nsF;
                render();
                frames++;
            }

            glfwPollEvents();

            if (System.currentTimeMillis() - timer >= 1000)
            {
                if (properties.printFPS_UPS)
                    System.out.println("Updates: " + updates + ", Frames: " + frames);

                timer += 1000;
                updates = 0;
                frames = 0;
            }
        }

        dispose();

        // Dispose of all Mods
        for (ModRef ref : ModRegistry.enabled())
            ref.mod.unload();
    }

    @Override
    public Properties properties()
    {
        return (Properties) properties;
    }

    public static class Properties extends FluxCore.Properties
    {
        public boolean windowVisibleOnCreate = false;
    }
}
