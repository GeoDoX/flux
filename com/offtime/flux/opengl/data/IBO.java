package com.offtime.flux.opengl.data;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;

/**
 * Created by GeoDoX on 2018-01-08.
 */
public class IBO extends Bindable
{
    private int[] data;

    public IBO(int length)
    {
        super(glGenBuffers());
        this.data = new int[length];
    }

    public IBO(int[] data)
    {
        super(glGenBuffers());

        data(data);
    }

    @Override
    public void bind()
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.handle);
    }

    @Override
    public void unbind()
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public int[] data()
    {
        return data;
    }

    public void data(int[] data)
    {
        this.data = data;

        bind();

        glBufferData(this.handle, data, GL_STATIC_DRAW);

        unbind();
    }
}
