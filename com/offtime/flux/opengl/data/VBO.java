package com.offtime.flux.opengl.data;

import static org.lwjgl.opengl.GL15.*;

/**
 * Created by GeoDoX on 2018-01-08.
 */
public class VBO extends Bindable
{
    private float[] data;

    public VBO(int length)
    {
        super(glGenBuffers());

        this.data = new float[length];
    }

    public VBO(float[] data)
    {
        super(glGenBuffers());

        data(data);
    }

    @Override
    public void bind()
    {
        glBindBuffer(GL_ARRAY_BUFFER, this.handle);
    }

    @Override
    public void unbind()
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    public float[] data()
    {
        return data;
    }

    public void data(float[] data)
    {
        this.data = data;

        bind();

        glBufferData(this.handle, data, GL_STATIC_DRAW);

        unbind();
    }
}
