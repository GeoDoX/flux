package com.offtime.flux.opengl.data;

/**
 * Created by GeoDoX on 2018-01-08.
 */
public abstract class Bindable
{
    public final int handle;

    public Bindable(int handle)
    {
        this.handle = handle;
    }

    public abstract void bind();
    public abstract void unbind();
}
