package com.offtime.flux.opengl.util;

import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;

/**
 * Created by GeoDoX on 2018-01-08.
 */
public class GLFWUtils
{
    public static int bool(boolean b)
    {
        return b ? GLFW_TRUE : GLFW_FALSE;
    }

    public static boolean bool(int b)
    {
        return b == GLFW_TRUE;
    }
}
