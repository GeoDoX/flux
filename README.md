# Flux #

Flux is an abstract 2D/3D Game Engine written
in Java with wrappers for Swing and OpenGL.

Please see the [Wiki](https://bitbucket.org/GeoDoX/flux/wiki/) for instructions on how
get set up, how to contribute, as well as some
more information about what Flux is.